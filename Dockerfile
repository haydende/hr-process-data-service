FROM openjdk:17.0.2

COPY build/libs/hr-process-data-service-0.0.1-SNAPSHOT.jar /app.jar

CMD java -jar app.jar --spring.profiles.active=docker
