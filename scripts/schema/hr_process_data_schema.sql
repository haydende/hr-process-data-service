-- MySQL dump 10.18  Distrib 10.3.27-MariaDB, for debian-linux-gnu (aarch64)
--
-- Host: localhost    Database: HR_PROCESS
-- ------------------------------------------------------
-- Server version	10.3.27-MariaDB-0+deb10u1

CREATE SCHEMA hr_process_data;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

use hr_process_data

--
-- Table structure for table `document`
--
DROP TABLE IF EXISTS `document`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_type` varchar(255) DEFAULT NULL,
  `owner_employee_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` blob DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY (`owner_employee_id`),
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`owner_employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee`
--
DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `forename1` varchar(255) NOT NULL,
  `forename2` varchar(255) DEFAULT NULL,
  `forename3` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255),
  `office_phone` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `holidays` smallint(6),
  `job_title` varchar(255) NOT NULL,
  `line_manager_id` bigint(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT 'they/them',
  `ethnicity` varchar(255) DEFAULT 'N/A',
  `religion` varchar(255) DEFAULT 'N/A',
  `disability` varchar(255) DEFAULT 'N/A',
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `line_manager_id` (`line_manager_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`line_manager_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--
DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_thread_id` bigint(20) DEFAULT NULL,
  `content` MEDIUMTEXT DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_thread_id` (`message_thread_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`message_thread_id`) REFERENCES `message_thread` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_thread`
--
DROP TABLE IF EXISTS `message_thread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_thread` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message_thread_employee`
--
DROP TABLE IF EXISTS `message_thread_employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message_thread_employee` (
  `message_thread_id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  PRIMARY KEY (`message_thread_id`,`employee_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `message_thread_employee_ibfk_1` FOREIGN KEY (`message_thread_id`) REFERENCES `message_thread` (`id`),
  CONSTRAINT `message_thread_employee_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table 'document_access'
--
DROP TABLE IF EXISTS `document_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `document_access` (
    `document_id` bigint(20) NOT NULL,
    `employee_id` bigint(20) NOT NULL,
    `contributed` bit(1) DEFAULT b'0',
    PRIMARY KEY (`document_id`, `employee_id`),
    CONSTRAINT `document_access_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`),
    CONSTRAINT `document_access_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_type`
--
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `notification`
--
DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_type_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `published` bit(1) DEFAULT b'0',
  `published_datetime` datetime DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `notification_ibkf_1` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification_type_setting`
--
DROP TABLE IF EXISTS `notification_type_setting`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notification_type_setting` (
  `employee_id` bigint(20) NOT NULL,
  `notification_type_id` bigint(20) NOT NULL,
  `use_client` bit(1) DEFAULT b'1',
  `use_email` bit(1) DEFAULT b'1',
  `use_phone` bit(1) DEFAULT b'0',
  PRIMARY KEY (`employee_id`, `notification_type_id`),
  CONSTRAINT `notification_type_setting_ibkf_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `notification_type_setting_ibkf_2` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee_notification`
--
DROP TABLE IF EXISTS `employee_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `employee_notification` (
  `notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) NOT NULL,
  PRIMARY KEY (`notification_id`,`employee_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `employee_notification_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
  CONSTRAINT `employee_notification_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

GRANT ALL PRIVILEGES ON hr_process_data.* TO 'remote'@'%';