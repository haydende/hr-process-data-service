use HR_PROCESS;

-- create the tables first
create table user (
	id bigint primary key auto_increment,
	first_name varchar(255),
	last_name varchar(255),
	email varchar(255),
	phone varchar(255),
	password TEXT,
	birth_date datetime,
	gender varchar(255),
	ethnicity varchar(255),
	religion varchar(255),
	disability varchar(255),
	notification_settings_id bigint
);

create table employee (
	id bigint primary key auto_increment,
	user_id bigint,
	company_id bigint,
	internal_email varchar(255),
	holidays smallint,
	is_employer bit default 0
);

create table candidate (
	id bigint primary key auto_increment,
	user_id bigint
);

create table company (
	id bigint primary key auto_increment,
	name varchar(255),
	sector varchar(255)
);

create table application (
	id bigint primary key auto_increment,
	company_id bigint,
	candidate_id bigint,
	application_status_id bigint
);

create table application_status (
	id bigint primary key auto_increment,
	name varchar(255)
);

create table review (
	id bigint primary key auto_increment,
	user_id bigint,
	company_id bigint,
	title varchar(255),
	content varchar(2000),
	rating smallint
);

create table user_notification (
	notification_id bigint auto_increment,
	user_id bigint,
    primary key (notification_id, user_id)
);

create table notification (
	id bigint primary key auto_increment,
	title varchar(255),
	content varchar(2000),
	published bit default 0,
	published_date datetime,
	created_date datetime
);

create table notification_settings (
	id bigint primary key auto_increment,
	use_email bit default 1,
	use_phone bit default 1,
	cron varchar(255) default '* * */15 * *'
);

create table message_thread_user (
	message_thread_id bigint,
	user_id bigint,
    primary key (message_thread_id, user_id)
);

create table message_thread (
	id bigint primary key auto_increment,
	subject varchar(255),
	created_date datetime
);

create table message (
	id bigint primary key auto_increment,
	message_thread_id bigint,
	content varchar(2000),
	created_date datetime
);

create table document (
	id bigint primary key auto_increment,
	document_type_id bigint,
	user_id bigint,
	name varchar(255),
	content blob,
	created_date datetime,
	last_modified datetime
);

create table document_type (
	id bigint primary key auto_increment,
	name varchar(255)
);

CREATE TABLE document_access (
	document_id bigint,
    employee_id bigint,
    company_id bigint,
    write_access bit,
    PRIMARY KEY (document_id, employee_id),
    FOREIGN KEY (document_id) REFERENCES document(id),
    FOREIGN KEY (employee_id) REFERENCES employee(id),
    FOREIGN KEY (company_id) REFERENCES company(id)
);

-- Adding FOREIGN KEYS
alter table user
add foreign key (notification_settings_id) references notification_settings(id);

alter table employee
add foreign key (user_id) references user(id),
add foreign key (company_id) references company(id);

alter table candidate 
add foreign key (user_id) references user(id);

alter table application
add foreign key (company_id) references company(id),
add foreign key (candidate_notification_frequency_type_idid) references candidate(id),
add foreign key (application_status_id) references application_status(id);

alter table review
add foreign key (user_id) references user(id),
add foreign key (company_id) references company(id);

alter table user_notification 
add foreign key (notification_id) references notification(id),
add foreign key (user_id) references user(id);

alter table message_thread_user
add foreign key (message_thread_id) references message_thread(id),
add foreign key (user_id) references user(id);

alter table message 
add foreign key (message_thread_id) references message_thread(id);

alter table document 
add foreign key (user_id) references user(id),
add foreign key (company_id) references company(id);