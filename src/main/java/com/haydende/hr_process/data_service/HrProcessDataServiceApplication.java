package com.haydende.hr_process.data_service;

import com.haydende.hr_process.data_service.dto.MessageDTO;
import com.haydende.hr_process.data_service.model.Message;
import com.haydende.hr_process.data_service.model.id.MessageId;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.util.TimeZone;
import java.util.function.BiConsumer;

@SpringBootApplication
public class HrProcessDataServiceApplication {

	@PostConstruct
	public void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

	public static void main(String[] args) {
		SpringApplication.run(HrProcessDataServiceApplication.class, args);
	}

}