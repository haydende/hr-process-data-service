package com.haydende.hr_process.data_service.controller;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public abstract class AbstractController {

    protected static final Locale locale = Locale.getDefault();
    protected static final ResourceBundle strings = ResourceBundle.getBundle("localisation.Strings", locale);

    protected static String formatMessage(String patternName, Object... arguments) {
        String stringPattern = strings.getString(patternName);
        return MessageFormat.format(stringPattern, arguments);
    }

}
