package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.DocumentDTO;
import com.haydende.hr_process.data_service.model.Document;
import com.haydende.hr_process.data_service.model.DocumentAccess;
import com.haydende.hr_process.data_service.service.DocumentAccessService;
import com.haydende.hr_process.data_service.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/document")
@RequiredArgsConstructor
public class DocumentController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(DocumentController.class);
    private static final String ID = "Id";
    private static final String DOCUMENT = "Document";
    private static final String OWNER_EMPLOYEE_ID = "OwnerEmployeeId";
    private final DocumentService documentService;
    private final DocumentAccessService documentAccessService;

    @Autowired
    private final ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<Object> getDocumentByIdOrOwnerEmployeeId(
        @RequestParam(required = false) Long id,
        @RequestParam(value = "owner", required = false) Long ownerEmployeeId
    ) {
        LOG.debug(strings.getString("log.controller.document.get.received"), id, ownerEmployeeId);
        ResponseEntity<Object> responseEntity;

        // TODO: Complete this once credential management has been implemented
//      DocumentAccess documentAccess = documentAccessService.getByDocumentIdAndEmployeeId(id, employeeId);
//      if (documentAccess != null) {
//          Document retrievedDocument = documentService.findById(id);
//      }

        if (!Objects.isNull(id)) {
            Document retrievedDocument = documentService.findById(id);
            if (!Objects.isNull(retrievedDocument)) {
                DocumentDTO documentDTO = modelMapper.map(retrievedDocument, DocumentDTO.class);
                responseEntity = new ResponseEntity<>(documentDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.document.get.found"), id, ownerEmployeeId);
            } else {
                String message = formatMessage("response.controller.document.not-found", id, ownerEmployeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.document.get.not-found"), id, ownerEmployeeId);
            }
        } else if (!Objects.isNull(ownerEmployeeId)) {
            List<Document> retrievedDocuments = documentService.findAllByOwnerEmployeeId(ownerEmployeeId);
            if (!retrievedDocuments.isEmpty()) {
                List<DocumentDTO> retrievedDocumentDTOs = retrievedDocuments
                    .stream()
                    .map(document -> modelMapper.map(document, DocumentDTO.class))
                    .toList();
                responseEntity = new ResponseEntity<>(retrievedDocumentDTOs, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.document.get.found"), id, ownerEmployeeId);
            } else {
                String message = formatMessage("response.controller.document.not-found", id, ownerEmployeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.document.get.not-found"), id, ownerEmployeeId);
            }
        } else {
            String message = formatMessage("response.controller.document.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.document.get.missing-params"));
        }
        return responseEntity;
    }

    @PostMapping(value = "/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveNewDocument(@RequestBody DocumentDTO documentDTO) {
        LOG.debug(strings.getString("log.controller.document.post.received"), documentDTO);
        ResponseEntity<Object> responseEntity;
        if (Objects.isNull(documentDTO.getId())) {
            Document savedDocument = documentService.save(modelMapper.map(documentDTO, Document.class));
            DocumentDTO savedDocumentDTO = modelMapper.map(savedDocument, DocumentDTO.class);
            responseEntity = new ResponseEntity<>(savedDocumentDTO, HttpStatus.OK);
            LOG.debug(strings.getString("log.controller.document.post.saved"), savedDocument.getId());
        } else {
            String message = formatMessage("response.controller.document.id-present");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.document.post.id-present"));
        }
        return responseEntity;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveExistingDocument(@RequestBody DocumentDTO documentDTO) {
        LOG.debug(strings.getString("log.controller.document.put.received"), documentDTO);
        ResponseEntity<Object> responseEntity;
        if (!Objects.isNull(documentDTO.getId())) {
            Document documentToUpdate = documentService.findById(documentDTO.getId());
            if (!Objects.isNull(documentToUpdate)) {
                documentToUpdate.setName(documentDTO.getName() != null ? documentDTO.getName() : documentToUpdate.getName());
                documentToUpdate.setContent(documentDTO.getContent() != null ? documentDTO.getContent() : documentToUpdate.getContent());
                documentToUpdate.setDocumentType(documentDTO.getDocumentType() != null ? documentDTO.getDocumentType() : documentToUpdate.getDocumentType());

                Document savedDocument = documentService.save(modelMapper.map(documentDTO, Document.class));
                DocumentDTO savedDocumentDTO = modelMapper.map(savedDocument, DocumentDTO.class);

                responseEntity = new ResponseEntity<>(savedDocumentDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.document.put.saved"), savedDocument.getId());
            } else {
                String message = formatMessage("response.controller.document.id-field-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.document.put.id-incorrect"));
            }
        } else {
            String message = formatMessage("response.controller.document.id-field-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.document.put.id-missing"));
        }
        return responseEntity;
    }

    @DeleteMapping()
    public ResponseEntity<Object> deleteDocumentById(@RequestParam(required = false) Long id) {
        LOG.debug(strings.getString("log.controller.document.delete.received"), id);
        ResponseEntity<Object> responseEntity;
        if (!Objects.isNull(id)) {
            Document documentToDelete = documentService.findById(id);
            if (!Objects.isNull(documentToDelete)) {
                documentService.deleteById(id);
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.document.delete.deleted"), id);
            } else {
                String message = formatMessage("response.controller.document.id-incorrect", id);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.document.delete.not-found"), id);
            }
        } else {
            String message = formatMessage("response.controller.document.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.document.delete.id-missing"));
        }
        return responseEntity;
    }
}
