package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.EmployeeDTO;
import com.haydende.hr_process.data_service.model.Employee;
import com.haydende.hr_process.data_service.model.NotificationTypeSetting;
import com.haydende.hr_process.data_service.service.EmployeeService;
import com.haydende.hr_process.data_service.service.NotificationTypeSettingService;
import lombok.RequiredArgsConstructor;
import org.apache.groovy.parser.antlr4.util.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/employee")
@RequiredArgsConstructor
public class EmployeeController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);
    private final EmployeeService employeeService;
    private final NotificationTypeSettingService ntsService;
    private final ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<Object> getEmployeeById(
        @RequestParam(required = false) Long id,
        @RequestParam(required = false) String name,
        @RequestParam(required = false) String email
    ) {
        LOG.debug(strings.getString("log.controller.employee.get.received"), id);
        ResponseEntity<Object> responseEntity;
        if (id != null) {
            Employee retrievedEmployee = employeeService.findById(id);
            if (!Objects.isNull(retrievedEmployee)) {
                EmployeeDTO employeeDTO = modelMapper.map(retrievedEmployee, EmployeeDTO.class);
                responseEntity = new ResponseEntity<>(employeeDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.employee.get.found"), id, name, email);
            } else {
                String message = formatMessage("response.controller.employee.not-found", id, name, email);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.get.not-found"), id, name, email);
            }
        } else if (!StringUtils.isEmpty(name) && !StringUtils.isEmpty(email)) {
            List<Employee> retrievedEmployees = employeeService.findAllByNameAndEmail(name, email);
            if (!retrievedEmployees.isEmpty()) {
                List<EmployeeDTO> employeeDTOs = retrievedEmployees
                    .stream()
                    .map(employee -> modelMapper.map(employee, EmployeeDTO.class))
                    .toList();
                responseEntity = new ResponseEntity<>(employeeDTOs, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.employee.get.found"), id, name, email);
            } else {
                String message = formatMessage("response.controller.employee.not-found", id, name, email);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.get.not-found"), id, name, email);
            }
        } else if (!StringUtils.isEmpty(name) && StringUtils.isEmpty(email)) {
            List<Employee> retrievedEmployees = employeeService.findAllByName(name);
            if (!retrievedEmployees.isEmpty()) {
                List<EmployeeDTO> employeeDTOs = retrievedEmployees
                    .stream()
                    .map(employee -> modelMapper.map(employee, EmployeeDTO.class))
                    .toList();
                responseEntity = new ResponseEntity<>(employeeDTOs, HttpStatus.OK);
                LOG.error(strings.getString("log.controller.employee.get.found"), id, name, email);
            } else {
                String message = formatMessage("response.controller.employee.not-found", id, name, email);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.get.not-found"));
            }
        } else if (!StringUtils.isEmpty(email) && StringUtils.isEmpty(name)) {
            List<Employee> retrievedEmployees = employeeService.findAllByEmail(email);
            if (!retrievedEmployees.isEmpty()) {
                List<EmployeeDTO> employeeDTOs = retrievedEmployees
                    .stream()
                    .map(employee -> modelMapper.map(employee, EmployeeDTO.class))
                    .toList();
                responseEntity = new ResponseEntity<>(employeeDTOs, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.employee.get.found"), id, name, email);
            } else {
                String message = formatMessage("response.controller.employee.not-found", id, name, email);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.get.not-found"), id, name, email);
            }
        } else {
            String message = formatMessage("response.controller.employee.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.employee.get.missing-params"));
        }
        return responseEntity;
    }

    @PostMapping(value = "/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveNewEmployee(@RequestBody EmployeeDTO employee) {
        LOG.debug(strings.getString("log.controller.employee.post.received"));
        ResponseEntity<Object> responseEntity;
        if (employee.getId() == null) {
            Employee savedEmployee = employeeService.save(modelMapper.map(employee, Employee.class));
            ntsService.createForEmployeeId(savedEmployee);
            responseEntity = new ResponseEntity<>(modelMapper.map(savedEmployee, EmployeeDTO.class), HttpStatus.OK);
            LOG.debug(strings.getString("log.controller.employee.post.saved"), savedEmployee.getId());
        } else {
            String message = formatMessage("response.controller.employee.id-present");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.employee.post.id-present"));
        }
        return responseEntity;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveExistingEmployee(@RequestBody EmployeeDTO employeeDTO) {
        LOG.debug(strings.getString("log.controller.employee.put.received"), employeeDTO);
        ResponseEntity<Object> responseEntity;
        if (employeeDTO.getId() != null) {
            Employee employeeToUpdate = employeeService.findById(employeeDTO.getId());
            if (!Objects.isNull(employeeToUpdate)) {
                applyDtoToModel(employeeDTO, employeeToUpdate);
                Employee savedEmployee = employeeService.save(employeeToUpdate);
                responseEntity = new ResponseEntity<>(modelMapper.map(savedEmployee, EmployeeDTO.class), HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.employee.put.saved"), savedEmployee.getId());
            } else {
                String message = formatMessage("response.controller.employee.id-field-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.put.id-incorrect"));
            }
        } else {
            String message = formatMessage("response.controller.employee.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.employee.put.id-missing"));
        }
        return responseEntity;
    }

    @DeleteMapping()
    public ResponseEntity<Object> deleteEmployeeById(@RequestParam(required = false) Long id) {
        LOG.debug(strings.getString("log.controller.employee.delete.received"), id);
        ResponseEntity<Object> responseEntity;
        if (!Objects.isNull(id)) {
            Employee employeeToDelete = employeeService.findById(id);
            if (!Objects.isNull(employeeToDelete)) {
                List<NotificationTypeSetting> ntsList = ntsService.findAllWithEmployeeId(employeeToDelete.getId());
                ntsService.deleteAll(ntsList);
                employeeService.deleteById(id);
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.employee.delete.deleted"), id);
            } else {
                String message = formatMessage("response.controller.employee.id-incorrect", id);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.employee.delete.not-found"), id);
            }
        } else {
            String message = formatMessage("response.controller.employee.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.employee.delete.id-missing"));
        }
        return responseEntity;
    }

    private void applyDtoToModel(EmployeeDTO employeeDTO, Employee employeeToSave) {
        employeeToSave.setForename1(employeeDTO.getForename1() != null ? employeeDTO.getForename1() : employeeToSave.getForename1());
        employeeToSave.setForename2(employeeDTO.getForename2() != null ? employeeDTO.getForename2() : employeeToSave.getForename2());
        employeeToSave.setForename3(employeeDTO.getForename3() != null ? employeeDTO.getForename3() : employeeToSave.getForename3());
        employeeToSave.setLastname(employeeDTO.getLastname() != null ? employeeDTO.getLastname() : employeeToSave.getLastname());
        employeeToSave.setEmail(employeeDTO.getEmail() != null ? employeeDTO.getEmail() : employeeToSave.getEmail());
        employeeToSave.setOfficePhone(employeeDTO.getOfficePhone() != null ? employeeDTO.getOfficePhone() : employeeToSave.getOfficePhone());
        employeeToSave.setMobilePhone(employeeDTO.getMobilePhone() != null ? employeeDTO.getMobilePhone() : employeeToSave.getMobilePhone());
        employeeToSave.setHolidays(employeeDTO.getHolidays() != null ? employeeDTO.getHolidays() : employeeToSave.getHolidays());
        employeeToSave.setJobTitle(employeeDTO.getJobTitle() != null ? employeeDTO.getJobTitle() : employeeToSave.getJobTitle());

        if (employeeDTO.getLineManagerId() != null) {
            Employee newLineManager = employeeService.findById(employeeDTO.getLineManagerId());
            employeeToSave.setLineManager(newLineManager != null ? newLineManager : employeeToSave.getLineManager());
        }

        employeeToSave.setBirthDate(employeeDTO.getBirthDate() != null ? employeeDTO.getBirthDate() : employeeToSave.getBirthDate());
        employeeToSave.setGender(employeeDTO.getGender() != null ? employeeDTO.getGender() : employeeToSave.getGender());
        employeeToSave.setEthnicity(employeeDTO.getEthnicity() != null ? employeeDTO.getEthnicity() : employeeToSave.getEthnicity());
        employeeToSave.setReligion(employeeDTO.getReligion() != null ? employeeDTO.getReligion() : employeeToSave.getReligion());
        employeeToSave.setDisability(employeeDTO.getDisability() != null ? employeeDTO.getDisability() : employeeToSave.getDisability());
    }
}
