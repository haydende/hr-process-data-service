package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.MessageDTO;
import com.haydende.hr_process.data_service.model.Message;
import com.haydende.hr_process.data_service.service.MessageService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/message")
@RequiredArgsConstructor
public class MessageController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(MessageController.class);
    private static final String ID = "Id";
    private static final String MESSAGE = "Message";
    private static final String MESSAGE_THREAD_ID = "MessageThreadId";
    private final Locale locale = Locale.getDefault();
    private final ResourceBundle strings = ResourceBundle.getBundle("localisation.Strings", locale);
    @Autowired
    private final MessageService messageService;
    @Autowired
    private final ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<Object> getMessageById(
        @RequestParam("messageThread") Long messageThreadId,
        @RequestParam(value = "id", required = false) Long messageId
    ) {
        LOG.debug(strings.getString("log.controller.message.get.received"), messageId, messageThreadId);
        ResponseEntity<Object> responseEntity;
        if (messageThreadId != null) {
            if (messageId != null) {
                Message retrievedMessage = messageService.findById(messageThreadId, messageId);
                if (!Objects.isNull(retrievedMessage)) {
                    LOG.debug(strings.getString("log.controller.message.get.found"), messageId, messageThreadId);
                    MessageDTO messageDTO = modelMapper.map(retrievedMessage, MessageDTO.class);
                    responseEntity = new ResponseEntity<>(messageDTO, HttpStatus.OK);
                } else {
                    LOG.error(strings.getString("log.controller.message.get.not-found"), messageId, messageThreadId);
                    String message = formatMessage("response.controller.message.not-found", messageId, messageThreadId);
                    responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                }
            } else {
                List<Message> retrievedMessages = messageService.findAllByMessageThreadId(messageThreadId);
                if (!retrievedMessages.isEmpty()) {
                    LOG.debug(strings.getString("log.controller.message.get.found"), messageId, messageThreadId);
                    List<MessageDTO> retrievedMessageDTOs =
                        retrievedMessages
                            .stream()
                            .map(message -> modelMapper.map(message, MessageDTO.class))
                            .toList();
                    responseEntity = new ResponseEntity<>(retrievedMessageDTOs, HttpStatus.OK);
                } else {
                    LOG.debug(strings.getString("log.controller.message.get.not-found"), messageId, messageThreadId);
                    String message = formatMessage("response.controller.message.not-found", messageId, messageThreadId);
                    responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                }
            }
        } else {
            LOG.error(
                strings.getString("log.controller.message.get.missing-params"));
            String message = formatMessage("response.controller.message.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PostMapping(value = "/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveNewMessage(@RequestBody MessageDTO messageDTO) {
        LOG.debug(strings.getString("log.controller.message.post.received"), messageDTO);
        ResponseEntity<Object> responseEntity;
        if (messageDTO.getId() == null) {
            if (messageDTO.getMessageThreadId() != null) {
                Message messageToSave = modelMapper.map(messageDTO, Message.class);
                Message savedMessage = messageService.save(messageToSave);
                MessageDTO savedMessageDTO = modelMapper.map(savedMessage, MessageDTO.class);
                responseEntity = new ResponseEntity<>(savedMessageDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.message.post.saved"), savedMessage.getMessageId().getId(), savedMessage.getMessageId().getMessageThreadId());
            } else {
                LOG.error(strings.getString("log.controller.message.post.message-thread-missing"));
                String message = formatMessage("response.controller.message.message-thread-missing");
                responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            }
        } else {
            LOG.error(strings.getString("log.controller.message.post.id-present"));
            String message = formatMessage("response.controller.message.id-present");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveExistingMessage(
        @RequestBody MessageDTO messageDTO
    ) {
        LOG.debug(strings.getString("log.controller.message.put.received"), messageDTO);
        ResponseEntity<Object> responseEntity;
        if (messageDTO.getMessageThreadId() != null && messageDTO.getId() != null) {
            Message messageToUpdate = messageService.findById(messageDTO.getMessageThreadId(), messageDTO.getId());
            if (!Objects.isNull(messageToUpdate)) {
                if (messageDTO.getContent() != null) {
                    messageToUpdate.setContent(messageDTO.getContent());
                    Message updatedMessage = messageService.save(messageToUpdate);
                    MessageDTO updatedMessageDTO = modelMapper.map(updatedMessage, MessageDTO.class);
                    responseEntity = new ResponseEntity<>(updatedMessageDTO, HttpStatus.OK);
                    LOG.debug(strings.getString("log.controller.message.put.saved"));
                } else {
                    LOG.error(strings.getString("log.controller.message.put.content-missing"));
                    String message = formatMessage("response.controller.message.content-missing");
                    responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
                }
            } else {
                LOG.error(strings.getString("log.controller.message.put.id-incorrect"));
                String message = formatMessage("response.controller.message.id-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.message.put.id-missing"));
            String message = formatMessage("response.controller.message.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @DeleteMapping()
    public ResponseEntity<Object> deleteMessageById(
        @RequestParam("messageThread") Long messageThreadId,
        @RequestParam(value = "id", required = false) Long messageId
    ) {
        LOG.debug(strings.getString("log.controller.message.delete.received"), messageId, messageThreadId);
        ResponseEntity<Object> responseEntity;
        if (messageThreadId != null && messageId != null) {
            Message messageToDelete = messageService.findById(messageThreadId, messageId);
            if (!Objects.isNull(messageToDelete)) {
                messageService.deleteById(messageThreadId, messageId);
                LOG.debug(strings.getString("log.controller.message.delete.deleted"), messageId, messageThreadId);
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.message.delete.not-found"), messageId, messageThreadId);
                String message = formatMessage("response.controller.message.not-found", messageId, messageThreadId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
           LOG.error(strings.getString("log.controller.message.delete.id-missing"));
           String message = formatMessage("response.controller.message.id-missing");
           responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

}
