package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.MessageThreadDTO;
import com.haydende.hr_process.data_service.model.MessageThread;
import com.haydende.hr_process.data_service.service.MessageThreadService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/message-thread")
@RequiredArgsConstructor
public class MessageThreadController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(MessageThreadController.class);
    private static final String ID = "Id";
    private static final String MESSAGE_THREAD = "MessageThread";
    private static final String EMPLOYEE_ID = "EmployeeId";
    private final Locale locale = Locale.getDefault();
    private final ResourceBundle strings = ResourceBundle.getBundle("localisation.Strings", locale);
    private final MessageThreadService messageThreadService;

    @Autowired
    private final ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<Object> getMessageThread(
        @RequestParam(required = false) Long id,
        @RequestParam(value = "employee", required = false) Long employeeId
    ) {
        LOG.debug(strings.getString("log.controller.message-thread.get.received"), id, employeeId);
        ResponseEntity<Object> responseEntity;
        if (employeeId == null && id != null) {
            MessageThread retrievedMessageThread = messageThreadService.findById(id);
            if (retrievedMessageThread != null) {
                LOG.debug(strings.getString("log.controller.message-thread.get.found"), id, employeeId);
                MessageThreadDTO retrievedMessageThreadDTO = modelMapper.map(retrievedMessageThread, MessageThreadDTO.class);
                responseEntity = new ResponseEntity<>(retrievedMessageThreadDTO, HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.message-thread.get.not-found"), id, employeeId);
                String message = formatMessage("response.controller.message-thread.not-found", id, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else if (employeeId != null) {
            List<MessageThread> retrievedMessageThreads = messageThreadService.findAllWithEmployeeId(employeeId);
            if (!retrievedMessageThreads.isEmpty()) {
                List<MessageThreadDTO> retrievedMessageThreadDTOs =
                    retrievedMessageThreads
                        .stream()
                        .map(msgTh -> modelMapper.map(msgTh, MessageThreadDTO.class))
                        .toList();
                if (id != null) {
                    List<MessageThreadDTO> filteredMessageThreads = retrievedMessageThreadDTOs
                        .stream()
                        .filter(messageThread -> Objects.equals(messageThread.getId(), id))
                        .toList();
                    if (!filteredMessageThreads.isEmpty()) {
                        LOG.debug(strings.getString("log.controller.message-thread.get.found"), id, employeeId);
                        responseEntity = new ResponseEntity<>(filteredMessageThreads, HttpStatus.OK);
                    } else {
                        LOG.error(strings.getString("log.controller.message-thread.get.not-found"), id, employeeId);
                        String message = formatMessage("response.controller.message-thread.not-found", id, employeeId);
                        responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                    }
                } else {
                    LOG.debug(strings.getString("log.controller.message-thread.get.found"), id, employeeId);
                    responseEntity = new ResponseEntity<>(retrievedMessageThreadDTOs, HttpStatus.OK);
                }
            } else {
                LOG.error(strings.getString("log.controller.message-thread.get.not-found"), id, employeeId);
                String message = formatMessage("response.controller.message-thread.not-found", id, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.message-thread.get.missing-params"));
            String message = formatMessage("response.controller.message-thread.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PostMapping(value = "/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveNewMessageThread(@RequestBody MessageThreadDTO messageThreadDTO) {
        LOG.debug(strings.getString("log.controller.message-thread.post.received"), messageThreadDTO);
        if (messageThreadDTO.getId() == null) {
            MessageThread messageThreadAsModel = modelMapper.map(messageThreadDTO, MessageThread.class);
            MessageThread savedMessageThread = messageThreadService.save(messageThreadAsModel);
            MessageThreadDTO savedMessageThreadDTO = modelMapper.map(savedMessageThread, MessageThreadDTO.class);
            LOG.debug(strings.getString("log.controller.message-thread.post.saved"), savedMessageThread.getId());
            return new ResponseEntity<>(savedMessageThreadDTO, HttpStatus.OK);
        } else {
            LOG.error(strings.getString("log.controller.message-thread.post.id-present"), MESSAGE_THREAD);
            String message = formatMessage("response.controller.message-thread.id-present", MESSAGE_THREAD);
            return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveExistingMessageThread(@RequestBody MessageThreadDTO messageThreadDTO) {
        LOG.debug(strings.getString("log.controller.message-thread.put.received"), messageThreadDTO);
        ResponseEntity<Object> responseEntity;
        if (messageThreadDTO.getId() != null) {
            MessageThread retrievedMessageThread = messageThreadService.findById(messageThreadDTO.getId());
            if (retrievedMessageThread != null) {
                if (messageThreadDTO.getSubject() != null) {
                    retrievedMessageThread.setSubject(messageThreadDTO.getSubject());
                    MessageThread updatedMessageThread = messageThreadService.save(retrievedMessageThread);
                    messageThreadDTO = modelMapper.map(updatedMessageThread, MessageThreadDTO.class);
                    responseEntity = new ResponseEntity<>(messageThreadDTO, HttpStatus.OK);
                    LOG.debug(strings.getString("log.controller.message-thread.put.saved"));
                } else {
                    LOG.error(strings.getString("log.controller.message-thread.put.missing-subject"));
                    String message = formatMessage("response.controller.message-thread.missing-subject");
                    responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
                }
            } else {
                LOG.error(strings.getString("log.controller.message-thread.put.id-incorrect"));
                String message = formatMessage("response.controller.message-thread.id-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.message-thread.put.id-missing"));
            String message = formatMessage("response.controller.message-thread.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @DeleteMapping()
    public ResponseEntity<Object> deleteMessageThreadById(
        @RequestParam(required = false) Long id,
        @RequestParam(value = "employee", required = false) Long employeeId
    ) {
        LOG.debug(strings.getString("log.controller.message-thread.delete.received"), id, employeeId);
        ResponseEntity<Object> responseEntity;
        if (employeeId != null && id == null) {
            List<MessageThread> retrievedMessageThreads = messageThreadService.findAllWithEmployeeId(employeeId);
            if (!retrievedMessageThreads.isEmpty()) {
                retrievedMessageThreads.forEach(msgTh -> messageThreadService.deleteById(msgTh.getId()));
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.message-thread.delete.deleted"), id, employeeId);
            } else {
                LOG.error(strings.getString("log.controller.message-thread.delete.not-found"), id, employeeId);
                String message = formatMessage("response.controller.message-thread.missing-params");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);    
            }
        } else if (id != null) {
            MessageThread retrievedMessageThread = messageThreadService.findById(id);
            if (retrievedMessageThread != null) {
                messageThreadService.deleteById(retrievedMessageThread.getId());
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.message-thread.delete.deleted"), id, messageThreadService);
            } else {
                LOG.error(strings.getString("log.controller.message-thread.delete.not-found"), id, employeeId);
                String message = formatMessage("response.controller.message-thread.not-found", id, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.message-thread.delete.missing-params"));
            String message = formatMessage("response.controller.message-thread.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }
}
