package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.NotificationDTO;
import com.haydende.hr_process.data_service.model.Notification;
import com.haydende.hr_process.data_service.service.EmployeeNotificationService;
import com.haydende.hr_process.data_service.service.NotificationService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/notification")
@RequiredArgsConstructor
public class NotificationController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(NotificationController.class);
    private final NotificationService notificationService;
    private final EmployeeNotificationService employeeNotificationService;
    private final ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<Object> getNotification(
        @RequestParam(value = "id", required = false) Long notificationId,
        @RequestParam(value = "employee", required = false) Long employeeId
    ) {
        LOG.debug(strings.getString("log.controller.notification.get.received"), notificationId, employeeId);
        ResponseEntity<Object> responseEntity;
        if (notificationId != null) {
            Notification notification = notificationService.findById(notificationId);
            if (notification != null) {
                LOG.debug(strings.getString("log.controller.notification.get.found"), notificationId, employeeId);
                NotificationDTO notificationDTO = modelMapper.map(notification, NotificationDTO.class);
                responseEntity = new ResponseEntity<>(notificationDTO, HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.notification.get.not-found"), notificationId, employeeId);
                String message =
                    formatMessage("response.controller.notification.not-found", notificationId, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else if (employeeId != null) {
            List<Notification> notifications = notificationService.findAllWithEmployeeId(employeeId);
            if (!notifications.isEmpty()) {
                List<NotificationDTO> notificationDTOs =
                    notifications
                        .stream()
                        .map(notification -> modelMapper.map(notification, NotificationDTO.class))
                        .toList();
                responseEntity = new ResponseEntity<>(notificationDTOs, HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.notification.get.not-found"), notificationId, employeeId);
                String message = formatMessage("response.controller.notification.not-found", notificationId, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.notification.get.missing-params"));
            String message = formatMessage("response.controller.notification.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PostMapping(value = "/new", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveNewNotification(@RequestBody NotificationDTO notificationDTO) {
        LOG.debug(strings.getString("log.controller.notification.post.received"), notificationDTO);
        ResponseEntity<Object> responseEntity;
        if (notificationDTO.getId() == null) {
            Notification notificationAsModel = modelMapper.map(notificationDTO, Notification.class);
            Notification savedNotification = notificationService.save(notificationAsModel);
            notificationDTO = modelMapper.map(savedNotification, NotificationDTO.class);
            responseEntity = new ResponseEntity<>(notificationDTO, HttpStatus.OK);
            LOG.debug(strings.getString("log.controller.notification.post.saved"), savedNotification.getId());
        } else {
            String message = formatMessage("response.controller.notification.id-present");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.notification.post.id-present"));
        }
        return responseEntity;
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveExistingNotification(@RequestBody NotificationDTO notificationDTO) {
        LOG.debug(strings.getString("log.controller.notification.put.received"), notificationDTO);
        ResponseEntity<Object> responseEntity;
        if (!Objects.isNull(notificationDTO.getId())) {
            Notification retrievedNotification = notificationService.findById(notificationDTO.getId());
            if (!Objects.isNull(retrievedNotification)) {
                Notification notificationAsModel = modelMapper.map(notificationDTO, Notification.class);
                Notification savedNotification = notificationService.save(notificationAsModel);
                notificationDTO = modelMapper.map(savedNotification, NotificationDTO.class);
                responseEntity = new ResponseEntity<>(notificationDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.notification.put.saved"), notificationDTO.getId());
            } else {
                String message = formatMessage("response.controller.notification.id-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.notification.put.id-incorrect"));
            }
        } else {
            String message = formatMessage("response.controller.notification.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.notification.put.id-missing"));
        }
        return responseEntity;
    }

    @DeleteMapping()
    public ResponseEntity<Object> deleteNotification(
        @RequestParam(required = false) Long id,
        @RequestParam(value = "employee", required = false) Long employeeId
    ) {
        LOG.debug(strings.getString("log.controller.notification.delete.received"), id, employeeId);
        ResponseEntity<Object> responseEntity;
        if (!Objects.isNull(id)) {
            Notification notificationToDelete = notificationService.findById(id);
            if (!Objects.isNull(notificationToDelete)) {
                notificationService.deleteById(id);
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.notification.delete.deleted"), id, employeeId);
            } else {
                String message = formatMessage("response.controller.notification.not-found", id, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.notification.delete.not-found"), id, employeeId);
            }
        } else if (!Objects.isNull(employeeId)) {
            List<Notification> notificationsForEmployee = notificationService.findAllWithEmployeeId(employeeId);
            if (!notificationsForEmployee.isEmpty()) {
                for (Notification notification : notificationsForEmployee) {
                    employeeNotificationService.deleteById(employeeId, notification.getId());
                    LOG.debug(strings.getString("log.controller.notification.delete.deleted-for-employee"), employeeId, notification.getId());
                }
                responseEntity = new ResponseEntity<>(HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.notification.delete.deleted"), id, employeeId);
            } else {
                String message = formatMessage("response.controller.notification.not-found", id, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
                LOG.error(strings.getString("log.controller.notification.delete.not-found"), id, employeeId);
            }
        } else {
            String message = formatMessage("response.controller.notification.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
            LOG.error(strings.getString("log.controller.notification.delete.missing-params"));
        }
        return responseEntity;
    }
}
