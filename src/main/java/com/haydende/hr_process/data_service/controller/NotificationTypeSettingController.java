package com.haydende.hr_process.data_service.controller;

import com.haydende.hr_process.data_service.dto.NotificationTypeSettingDTO;
import com.haydende.hr_process.data_service.model.NotificationTypeSetting;
import com.haydende.hr_process.data_service.repository.NotificationTypeRepository;
import com.haydende.hr_process.data_service.service.EmployeeService;
import com.haydende.hr_process.data_service.service.NotificationTypeSettingService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/notification-type-setting")
@RequiredArgsConstructor
public class NotificationTypeSettingController extends AbstractController {

    private static final Logger LOG = LoggerFactory.getLogger(NotificationController.class);
    private static final String NOTIFICATION_TYPE = "notificationType";
    private static final String EMPLOYEE = "employee";
    private final NotificationTypeSettingService notificationTypeSettingService;
    private final NotificationTypeRepository notificationTypeRepository;
    private final ModelMapper modelMapper;

    @GetMapping()
    public ResponseEntity<Object> getByEmployeeOrNotificationType(
        @RequestParam(NOTIFICATION_TYPE) Long notificationTypeId,
        @RequestParam(EMPLOYEE) Long employeeId
    ) {
        LOG.debug(strings.getString("log.controller.notification-type-setting.get.received"), notificationTypeId, employeeId);
        ResponseEntity<Object> responseEntity;
        if (notificationTypeId != null && employeeId != null) {
            NotificationTypeSetting nts = notificationTypeSettingService.findByTypeIdAndEmployeeId(employeeId, notificationTypeId);
            if (nts != null) {
                LOG.debug(strings.getString("log.controller.notification-type-setting.get.found"), notificationTypeId, employeeId);
                NotificationTypeSettingDTO ntsDTO = modelMapper.map(nts, NotificationTypeSettingDTO.class);
                responseEntity = new ResponseEntity<>(ntsDTO, HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.notification-type-setting.get.not-found"), notificationTypeId, employeeId);
                String message = formatMessage("response.controller.notification-type-setting.not-found", notificationTypeId, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else if (employeeId != null) {
            List<NotificationTypeSetting> ntsList = notificationTypeSettingService.findAllWithEmployeeId(employeeId);
            if (ntsList != null && !ntsList.isEmpty()) {
                LOG.debug(strings.getString("log.controller.notification-type-setting.get.found"), notificationTypeId, employeeId);
                List<NotificationTypeSettingDTO> ntsListDTO = ntsList.stream().map(nts -> modelMapper.map(nts, NotificationTypeSettingDTO.class)).toList();
                responseEntity = new ResponseEntity<>(ntsListDTO, HttpStatus.OK);
            } else {
                LOG.error(strings.getString("log.controller.notification-type-setting.get.not-found"), notificationTypeId, employeeId);
                String message = formatMessage("response.controller.notification-type-setting.not-found", notificationTypeId, employeeId);
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.notification-type-setting.get.missing-params"));
            String message = formatMessage("response.controller.notification-type-setting.missing-params");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

    @PutMapping()
    public ResponseEntity<Object> updateExisting(
        @RequestBody NotificationTypeSettingDTO ntsDTO
    ) {
        LOG.debug(strings.getString("log.controller.notification-type-setting.put.received"), ntsDTO);
        ResponseEntity<Object> responseEntity;
        if (ntsDTO.getNotificationTypeId() != null && ntsDTO.getEmployeeId() != null) {

            NotificationTypeSetting nts = notificationTypeSettingService.findByTypeIdAndEmployeeId(ntsDTO.getEmployeeId(), ntsDTO.getNotificationTypeId());
            if (nts != null) {
                nts.setUseClient(ntsDTO.getUseClient() != null ? ntsDTO.getUseClient() : nts.getUseClient());
                nts.setUsePhone(ntsDTO.getUsePhone() != null ? ntsDTO.getUsePhone() : nts.getUsePhone());
                nts.setUseEmail(ntsDTO.getUseEmail() != null ? ntsDTO.getUseEmail() : nts.getUseEmail());

                nts = notificationTypeSettingService.save(nts);
                ntsDTO = modelMapper.map(nts, NotificationTypeSettingDTO.class);
                responseEntity = new ResponseEntity<>(ntsDTO, HttpStatus.OK);
                LOG.debug(strings.getString("log.controller.notification-type-setting.put.saved"), ntsDTO.getNotificationTypeId(), ntsDTO.getEmployeeId());
            } else {
                LOG.error(strings.getString("log.controller.notification-type-setting.put.id-incorrect"));
                String message = formatMessage("response.controller.notification-type-setting.id-incorrect");
                responseEntity = new ResponseEntity<>(message, HttpStatus.NOT_FOUND);
            }
        } else {
            LOG.error(strings.getString("log.controller.notification-type-setting.put.id-missing"));
            String message = formatMessage("response.controller.notification-type-setting.id-missing");
            responseEntity = new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
        }
        return responseEntity;
    }

}