package com.haydende.hr_process.data_service.dto;

import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class DocumentDTO {

    private Long id;

    private Long ownerEmployeeId;

    private String documentType;

    private String name;

    private Byte[] content;

    private Timestamp createdDatetime;

    private Timestamp lastModifiedDatetime;
}
