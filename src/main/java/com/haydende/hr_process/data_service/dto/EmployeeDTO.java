package com.haydende.hr_process.data_service.dto;

import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class EmployeeDTO {

    private Long id;

    private String forename1;

    private String forename2;

    private String forename3;

    private String lastname;

    private String email;

    private String officePhone;

    private String mobilePhone;

    private Integer holidays;

    private String jobTitle;

    private Long lineManagerId;

    private Timestamp birthDate;

    private String gender;

    private String ethnicity;

    private String religion;

    private String disability;

    private Timestamp createdDatetime;

}
