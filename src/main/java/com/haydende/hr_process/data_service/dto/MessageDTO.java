package com.haydende.hr_process.data_service.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class MessageDTO {

    private Long id;

    @NotBlank(message = "Content is required for Message objects")
    private String content;

    private Long messageThreadId;

    private Timestamp createdDatetime;

    private Timestamp lastModifiedDatetime;
}
