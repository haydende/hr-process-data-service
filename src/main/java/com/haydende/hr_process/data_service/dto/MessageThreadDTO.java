package com.haydende.hr_process.data_service.dto;

import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter @ToString
public class MessageThreadDTO {

    private Long id;

    private String subject;

    private Timestamp lastModifiedDatetime;

    private Timestamp createdDatetime;
}
