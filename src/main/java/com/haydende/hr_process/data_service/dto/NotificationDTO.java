package com.haydende.hr_process.data_service.dto;

import lombok.*;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class NotificationDTO {

    private Long id;

    private Long notificationTypeId;

    private String title;

    private String content;

    private Boolean published;

    private Timestamp publishedDatetime;

    private Timestamp lastModifiedDatetime;

    private Timestamp createdDatetime;

}
