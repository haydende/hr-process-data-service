package com.haydende.hr_process.data_service.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class NotificationTypeSettingDTO {

    private Long notificationTypeId;

    private Long employeeId;

    private Boolean useClient;

    private Boolean useEmail;

    private Boolean usePhone;
}
