package com.haydende.hr_process.data_service.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "Document")
@Builder
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(
        name = "owner_employee_id",
        referencedColumnName = "id",
        insertable = false,
        updatable = false
    )
    private Employee ownerEmployee;

    @OneToMany(
        targetEntity = DocumentAccess.class,
        mappedBy = "documentId",
        fetch = FetchType.LAZY,
        cascade = CascadeType.ALL
    )
    private List<DocumentAccess> documentAccessEntries;

    @Column
    private String documentType;

    @Column
    private String name;

    @Column
    private Byte[] content;

    @Column
    private Timestamp createdDatetime;

    @Column
    private Timestamp lastModifiedDatetime;
}
