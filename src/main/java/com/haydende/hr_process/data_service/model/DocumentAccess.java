package com.haydende.hr_process.data_service.model;

import com.haydende.hr_process.data_service.model.id.DocumentAccessId;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "document_access")
@IdClass(DocumentAccessId.class)
@Builder @Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class DocumentAccess {

    @Id
    @Column(name = "employee_id")
    private Long employeeId;

    @Id
    @Column(name = "document_id")
    private Long documentId;

    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(
        name = "employee_id",
        referencedColumnName = "id",
        insertable = false,
        updatable = false
    )
    private Employee employee;

    @ManyToOne(targetEntity = Document.class)
    @JoinColumn(
        name = "document_id",
        referencedColumnName = "id",
        insertable = false,
        updatable = false
    )
    private Document document;

    @Column
    private Boolean contributed;

}
