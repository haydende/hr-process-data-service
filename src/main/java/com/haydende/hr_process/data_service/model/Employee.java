package com.haydende.hr_process.data_service.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "employee")
@Builder
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "forename1")
    private String forename1;

    @Column(name = "forename2")
    private String forename2;

    @Column(name = "forename3")
    private String forename3;

    @Column(name = "lastname")
    private String lastname;

    @Column(name = "email")
    private String email;

    @Column(name = "office_phone")
    private String officePhone;

    @Column(name = "mobile_phone")
    private String mobilePhone;

    @Column
    private String password;

    @Column
    private Integer holidays;

    @Column
    private String jobTitle;

    @OneToOne
    private Employee lineManager;

    @Column
    private Timestamp birthDate;

    @Column
    private String gender;

    @Column
    private String ethnicity;

    @Column
    private String religion;

    @Column
    private String disability;

    @Column
    private Timestamp createdDatetime;

    @OneToMany(mappedBy = "ownerEmployee")
    private List<Document> documents;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "employee_notification",
        joinColumns = {
            @JoinColumn(name = "employee_id", referencedColumnName = "id",
                        nullable = false, updatable = false
            )
        },
        inverseJoinColumns = {
            @JoinColumn(name = "notification_id", referencedColumnName = "id",
                        nullable = false, updatable = false
            )
        }
    )
    private List<Notification> notifications;

    @OneToMany(targetEntity = NotificationTypeSetting.class, mappedBy = "employeeId", cascade = CascadeType.ALL)
    private List<NotificationTypeSetting> notificationTypeSettings;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "message_thread_employee",
        joinColumns = {
                @JoinColumn(name = "message_thread_id", referencedColumnName = "id",
                            nullable = false, updatable = false
            )
        },
        inverseJoinColumns = {
            @JoinColumn(name = "employee_id", referencedColumnName = "id",
                        nullable = false, updatable = false
            )
        }
    )
    private List<MessageThread> messageThreads;

}
