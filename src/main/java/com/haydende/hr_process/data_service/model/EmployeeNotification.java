package com.haydende.hr_process.data_service.model;

import com.haydende.hr_process.data_service.model.id.EmployeeNotificationId;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "employee_notification")
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class EmployeeNotification implements Serializable {

    @EmbeddedId
    private EmployeeNotificationId employeeNotificationId;

    @ManyToOne(targetEntity = Employee.class, fetch = FetchType.LAZY)
    @MapsId("employeeId")
    private Employee employee;

    @ManyToOne(targetEntity = Notification.class, fetch = FetchType.LAZY)
    @MapsId("notificationId")
    private Notification notification;

}
