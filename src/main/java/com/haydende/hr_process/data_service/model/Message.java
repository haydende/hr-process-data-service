package com.haydende.hr_process.data_service.model;

import com.haydende.hr_process.data_service.model.id.MessageId;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "message")
@Builder
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class Message implements Serializable {

    @EmbeddedId
    private MessageId messageId;

    @Column(length = 2000)
    private String content;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("messageThreadId")
    private MessageThread messageThread;

    @Column
    private Timestamp createdDatetime;

    @Column
    private Timestamp lastModifiedDatetime;

}
