package com.haydende.hr_process.data_service.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "message_thread")
@Builder
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class MessageThread {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String subject;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "message_thread_employee",
        joinColumns = {
            @JoinColumn(name = "message_thread_id", referencedColumnName = "id",
                nullable = false, updatable = false
            )
        },
        inverseJoinColumns = {
            @JoinColumn(name = "employee_id", referencedColumnName = "id",
                nullable = false, updatable = false
            )
        }
    )
    private List<Employee> employees;

    @OneToMany(mappedBy = "messageThread", cascade = CascadeType.ALL)
    private List<Message> messages;

    @Column
    private Timestamp createdDatetime;

    @Column
    private Timestamp lastModifiedDatetime;

}
