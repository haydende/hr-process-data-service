package com.haydende.hr_process.data_service.model;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "Notification")
@Builder @Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
        name = "employee_notification",
        joinColumns = {
            @JoinColumn(name = "employee_id", referencedColumnName = "id",
                nullable = false, updatable = false
            )
        },
        inverseJoinColumns = {
            @JoinColumn(name = "notification_id", referencedColumnName = "id",
                nullable = false, updatable = false
            )
        }
    )
    private List<Employee> employees;

    @OneToOne
    private NotificationType notificationType;

    @Column
    private String title;

    @Column(length = 2000)
    private String content;

    @Column
    private Boolean published;

    @Column
    private Timestamp publishedDatetime;

    @Column
    private Timestamp createdDatetime;

    @Column
    private Timestamp lastModifiedDatetime;
}
