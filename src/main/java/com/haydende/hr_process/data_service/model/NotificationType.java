package com.haydende.hr_process.data_service.model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "notification_type")
@NoArgsConstructor
@AllArgsConstructor
@Builder @Getter @Setter
public class NotificationType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

}
