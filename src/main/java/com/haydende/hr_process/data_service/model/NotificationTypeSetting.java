package com.haydende.hr_process.data_service.model;

import com.haydende.hr_process.data_service.model.id.NotificationTypeSettingId;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "notification_type_setting")
@IdClass(NotificationTypeSettingId.class)
@Builder
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class NotificationTypeSetting implements Serializable {

    @Id
    @Column(name = "employee_id")
    private Long employeeId;

    @Id
    @Column(name = "notification_type_id")
    private Long notificationTypeId;

    @ManyToOne(targetEntity = Employee.class)
    @JoinColumn(
        name = "employee_id",
        referencedColumnName = "id",
        insertable = false,
        updatable = false
    )
    private Employee employee;

    @ManyToOne(targetEntity = NotificationType.class)
    @JoinColumn(
        name = "notification_type_id",
        referencedColumnName = "id",
        insertable = false,
        updatable = false
    )
    private NotificationType notificationType;

    @Column
    private Boolean useClient;

    @Column
    private Boolean useEmail;

    @Column
    private Boolean usePhone;
}
