package com.haydende.hr_process.data_service.model.id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Table(name = "document_access")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class DocumentAccessId implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "document_id")
    private Long documentId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentAccessId that = (DocumentAccessId) o;
        return that.getEmployeeId().equals(getEmployeeId()) &&
               that.getDocumentId().equals(getDocumentId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, documentId);
    }

}
