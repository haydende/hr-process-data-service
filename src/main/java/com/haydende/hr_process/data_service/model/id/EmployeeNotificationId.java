package com.haydende.hr_process.data_service.model.id;

import com.haydende.hr_process.data_service.model.EmployeeNotification;
import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Table(name = "employee_notification")
@Getter @Setter @Builder
@NoArgsConstructor @AllArgsConstructor
public class EmployeeNotificationId implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "notification_id")
    private Long notificationId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeNotificationId that = (EmployeeNotificationId) o;
        return that.getEmployeeId().equals(getEmployeeId()) &&
               that.getNotificationId().equals(getNotificationId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, notificationId);
    }

}
