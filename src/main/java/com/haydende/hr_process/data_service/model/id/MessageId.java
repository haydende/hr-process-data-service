package com.haydende.hr_process.data_service.model.id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Table(name = "message")
@Getter @Setter
@NoArgsConstructor @AllArgsConstructor
public class MessageId implements Serializable {

    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "message_thread_id")
    private Long messageThreadId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageId that = (MessageId) o;
        return that.getId().equals(getId()) &&
                that.getMessageThreadId().equals(getMessageThreadId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, messageThreadId);
    }


}
