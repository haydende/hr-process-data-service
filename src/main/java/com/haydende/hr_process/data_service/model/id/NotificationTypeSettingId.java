package com.haydende.hr_process.data_service.model.id;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
@Table(name = "notification_type_setting")
@NoArgsConstructor
@AllArgsConstructor
@Getter @Setter
public class NotificationTypeSettingId implements Serializable {

    @Column(name = "employee_id")
    private Long employeeId;

    @Column(name = "notification_type_id")
    private Long notificationTypeId;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NotificationTypeSettingId that = (NotificationTypeSettingId) o;
        return that.getEmployeeId().equals(getEmployeeId()) &&
               that.getNotificationTypeId().equals(getNotificationTypeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(employeeId, notificationTypeId);
    }

}
