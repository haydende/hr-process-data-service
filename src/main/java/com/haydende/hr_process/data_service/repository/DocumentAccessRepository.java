package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.DocumentAccess;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DocumentAccessRepository extends JpaRepository<DocumentAccess, Long> {

    DocumentAccess getByDocumentIdAndEmployeeId(Long documentId, Long employeeId);

    List<DocumentAccess> getByDocumentId(Long documentId);

    List<DocumentAccess> getByEmployeeId(Long employeeId);

}

