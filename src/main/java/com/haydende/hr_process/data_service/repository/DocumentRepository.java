package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

    Optional<Document> findById(Long id);

    List<Document> findAllByOwnerEmployeeId(Long ownerEmployeeId);

}
