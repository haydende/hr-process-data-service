package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.EmployeeNotification;
import com.haydende.hr_process.data_service.model.id.EmployeeNotificationId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeNotificationRepository extends JpaRepository<EmployeeNotification, EmployeeNotificationId> {

}
