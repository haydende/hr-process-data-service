package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    List<Employee> findByEmailLike(String email);

    List<Employee> findByForename1LikeOrForename2LikeOrForename3LikeOrLastnameLike(
            String forename1Entry,
            String forename2Entry,
            String forename3Entry,
            String lastnameEntry
    );

    @Query(
        "SELECT e " +
        "FROM Employee e " +
        "WHERE " +
            "e.forename1 LIKE :name OR " +
            "e.forename2 LIKE :name OR " +
            "e.forename3 LIKE :name OR " +
            "e.lastname LIKE :name  OR " +
            "e.email LIKE :email "
    )
    List<Employee> findByNamesLikeAndEmailLike(@Param("name") String name, @Param("email") String email);
}
