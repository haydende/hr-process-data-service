package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.Message;
import com.haydende.hr_process.data_service.model.id.MessageId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageRepository extends JpaRepository<Message, MessageId> {

    @Query("SELECT message FROM Message message WHERE message.messageId = :messageId")
    Message findByMessageId(@Param("messageId") MessageId messageId);

    @Query("SELECT message FROM Message message WHERE message.messageId.messageThreadId = :messageThreadId")
    List<Message> findAllByMessageThreadId(@Param("messageThreadId") Long messageThreadId);

}
