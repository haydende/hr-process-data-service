package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.MessageThread;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageThreadRepository extends JpaRepository<MessageThread, Long> {

    @Query(
        value = "SELECT * FROM message_thread WHERE message_thread.id IN " +
                "(SELECT message_thread_id FROM message_thread_employee WHERE message_thread_employee.employee_id = :employeeId)",
        nativeQuery = true
    )
    List<MessageThread> findAllWithEmployeeId(@Param("employeeId") Long userId);
}
