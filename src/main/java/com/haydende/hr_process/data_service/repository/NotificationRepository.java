package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.Notification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long> {

    @Query(
        value = "SELECT * FROM notification WHERE notification.id IN (SELECT notification_id FROM employee_notification WHERE employee_notification.employee_id = :employeeId)", nativeQuery = true)
    List<Notification> findAllWithEmployeeId(@Param("employeeId") Long employeeId);

}
