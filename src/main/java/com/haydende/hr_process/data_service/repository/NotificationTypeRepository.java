package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.NotificationType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NotificationTypeRepository extends JpaRepository<NotificationType, Long> {
}
