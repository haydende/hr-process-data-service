package com.haydende.hr_process.data_service.repository;

import com.haydende.hr_process.data_service.model.NotificationTypeSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface NotificationTypeSettingRepository extends JpaRepository<NotificationTypeSetting, Long> {

    @Query("SELECT nts FROM NotificationTypeSetting nts WHERE nts.employee.id = ?1")
    List<NotificationTypeSetting> findByEmployeeId(Long employeeId);

}
