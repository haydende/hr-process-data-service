package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.DocumentAccess;

public interface DocumentAccessService {

    DocumentAccess getByDocumentIdAndEmployeeId(Long documentId, Long employeeId);
    DocumentAccess createAccess(Long documentId, Long employeeId);

    void removeAccess(Long documentId, Long employeeId);

}
