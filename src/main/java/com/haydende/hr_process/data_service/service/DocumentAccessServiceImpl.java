package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.DocumentAccess;
import com.haydende.hr_process.data_service.repository.DocumentAccessRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class DocumentAccessServiceImpl implements DocumentAccessService {

    private final DocumentAccessRepository documentAccessRepository;

    @Override
    public DocumentAccess getByDocumentIdAndEmployeeId(Long documentId, Long employeeId) {
        return documentAccessRepository.getByDocumentIdAndEmployeeId(documentId, employeeId);
    }

    @Override
    public DocumentAccess createAccess(Long documentId, Long employeeId) {
        DocumentAccess newAccessRecord = DocumentAccess
            .builder()
                .employeeId(employeeId)
                .documentId(documentId)
                .build();
        return documentAccessRepository.save(newAccessRecord);
    }

    @Override
    public void removeAccess(Long documentId, Long employeeId) {
        DocumentAccess toDelete = documentAccessRepository.getByDocumentIdAndEmployeeId(documentId, employeeId);
        documentAccessRepository.delete(toDelete);
    }
}
