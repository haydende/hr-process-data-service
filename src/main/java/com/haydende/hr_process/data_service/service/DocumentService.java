package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Document;

import java.util.List;

public interface DocumentService {

    Document findById(Long id);

    List<Document> findAllByOwnerEmployeeId(Long userId);

    Document save(Document document);

    void deleteById(Long id);
}
