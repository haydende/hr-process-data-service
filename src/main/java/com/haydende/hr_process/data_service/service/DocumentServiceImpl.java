package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Document;
import com.haydende.hr_process.data_service.repository.DocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {

    private final DocumentRepository documentRepository;

    @Override
    public Document findById(Long id) {
        return documentRepository.findById(id).orElse(null);
    }

    @Override
    public List<Document> findAllByOwnerEmployeeId(Long userId) {
        return documentRepository.findAllByOwnerEmployeeId(userId);
    }

    @Override
    public Document save(Document document) {
        Timestamp lastModifiedDatetime = new Timestamp(new Date().getTime());
        if (document.getId() == null) {
            document.setCreatedDatetime(lastModifiedDatetime);
        }
        document.setLastModifiedDatetime(lastModifiedDatetime);
        return documentRepository.save(document);
    }

    @Override
    public void deleteById(Long id) {
        documentRepository.deleteById(id);
    }
}
