package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.EmployeeNotification;
import org.springframework.stereotype.Service;

@Service
public interface EmployeeNotificationService {

    EmployeeNotification findById(Long employeeId, Long notificationId);

    void deleteById(Long employeeId, Long notificationId);

}
