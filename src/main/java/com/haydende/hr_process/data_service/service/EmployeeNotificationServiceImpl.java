package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.EmployeeNotification;
import com.haydende.hr_process.data_service.model.id.EmployeeNotificationId;
import com.haydende.hr_process.data_service.repository.EmployeeNotificationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeNotificationServiceImpl implements EmployeeNotificationService {

    private final EmployeeNotificationRepository employeeNotificationRepository;

    @Override
    public EmployeeNotification findById(Long employeeId, Long notificationId) {
        EmployeeNotificationId enId = EmployeeNotificationId
            .builder()
            .employeeId(employeeId)
            .notificationId(notificationId)
            .build();
        return employeeNotificationRepository.findById(enId).orElse(null);
    }

    @Override
    public void deleteById(Long employeeId, Long notificationId) {
        EmployeeNotificationId enId = EmployeeNotificationId
            .builder()
            .employeeId(employeeId)
            .notificationId(notificationId)
            .build();
        employeeNotificationRepository.deleteById(enId);
    }
}
