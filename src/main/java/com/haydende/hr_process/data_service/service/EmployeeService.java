package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Employee;

import java.util.List;

public interface EmployeeService {

    Employee findById(Long id);

    List<Employee> findAllByEmail(String email);

    List<Employee> findAllByName(String name);

    List<Employee> findAllByNameAndEmail(String name, String email);

    Employee save(Employee employee);

    void deleteById(Long id);
}
