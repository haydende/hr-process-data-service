package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Employee;
import com.haydende.hr_process.data_service.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Override
    public Employee findById(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    @Override
    public List<Employee> findAllByEmail(String email) {
        return employeeRepository.findByEmailLike(email);
    }

    @Override
    public List<Employee> findAllByName(String name) {
        return employeeRepository.findByForename1LikeOrForename2LikeOrForename3LikeOrLastnameLike(name, name, name, name);
    }

    @Override
    public List<Employee> findAllByNameAndEmail(String name, String email) {
        return employeeRepository.findByNamesLikeAndEmailLike(name, email);
    }

    @Override
    public Employee save(Employee employee) {
        employee.setPassword(""); // TODO: Implement some form of password generation
        Date currentDateTime = new Date();
        Timestamp currentTimeStamp = new Timestamp(currentDateTime.getTime());
        employee.setCreatedDatetime(currentTimeStamp);
        return employeeRepository.save(employee);
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }
}
