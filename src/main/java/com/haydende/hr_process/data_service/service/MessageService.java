package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Message;
import com.haydende.hr_process.data_service.model.id.MessageId;

import java.util.List;

public interface MessageService {

    Message findById(Long messageThreadId, Long id);

    List<Message> findAllByMessageThreadId(Long messageThreadId);

    Message save(Message message);

    void deleteById(Long messageThreadId, Long messageId);
}
