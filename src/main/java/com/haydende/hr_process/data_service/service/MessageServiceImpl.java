package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Message;
import com.haydende.hr_process.data_service.model.id.MessageId;
import com.haydende.hr_process.data_service.repository.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final Logger LOG = LoggerFactory.getLogger(MessageServiceImpl.class);
    private final MessageRepository messageRepository;

    @Override
    public Message findById(Long messageThreadId, Long id) {
        MessageId messageId = new MessageId(messageThreadId, id);
        return messageRepository.findByMessageId(messageId);
    }

    @Override
    public List<Message> findAllByMessageThreadId(Long messageThreadId) {
        return messageRepository.findAllByMessageThreadId(messageThreadId);
    }

    @Override
    public Message save(Message message) {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        if (message.getCreatedDatetime() == null) {
            message.setCreatedDatetime(timestamp);
        }
        message.setLastModifiedDatetime(timestamp);
        return messageRepository.save(message);
    }

    @Override
    public void deleteById(Long messageThreadId, Long id) {
        MessageId messageId = new MessageId(messageThreadId, id);
        messageRepository.deleteById(messageId);
    }
}
