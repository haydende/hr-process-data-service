package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.MessageThread;

import java.util.List;

public interface MessageThreadService {

    MessageThread findById(Long id);

    MessageThread save(MessageThread messageThread);

    void deleteById(Long id);

    List<MessageThread> findAllWithEmployeeId(Long userId);
}
