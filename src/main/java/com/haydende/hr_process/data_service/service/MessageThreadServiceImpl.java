package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.MessageThread;
import com.haydende.hr_process.data_service.repository.MessageThreadRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class MessageThreadServiceImpl implements MessageThreadService {

    private final MessageThreadRepository messageThreadRepository;

    @Override
    public MessageThread findById(Long id) {
        return messageThreadRepository.findById(id).orElse(null);
    }

    @Override
    public List<MessageThread> findAllWithEmployeeId(Long userId) {
        return messageThreadRepository.findAllWithEmployeeId(userId);
    }

    @Override
    public MessageThread save(MessageThread messageThread) {
        return messageThreadRepository.save(messageThread);
    }

    @Override
    public void deleteById(Long id) {
        messageThreadRepository.deleteById(id);
    }
}
