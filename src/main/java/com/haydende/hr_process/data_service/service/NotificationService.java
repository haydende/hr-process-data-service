package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Notification;

import java.util.List;

public interface NotificationService {

    Notification findById(Long id);

    List<Notification> findAllWithEmployeeId(Long userId);

    Notification save(Notification notification);

    void deleteById(Long id);

}
