package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Employee;
import com.haydende.hr_process.data_service.model.NotificationTypeSetting;

import java.util.List;

public interface NotificationTypeSettingService {

    NotificationTypeSetting findByTypeIdAndEmployeeId(Long employeeId, Long notificationTypeId);

    List<NotificationTypeSetting> findAllWithEmployeeId(Long employeeId);

    void createForEmployeeId(Employee employee);

    NotificationTypeSetting save(NotificationTypeSetting nts);

    void deleteAll(List<NotificationTypeSetting> ntsList);
}
