package com.haydende.hr_process.data_service.service;

import com.haydende.hr_process.data_service.model.Employee;
import com.haydende.hr_process.data_service.model.NotificationType;
import com.haydende.hr_process.data_service.model.NotificationTypeSetting;
import com.haydende.hr_process.data_service.repository.NotificationTypeRepository;
import com.haydende.hr_process.data_service.repository.NotificationTypeSettingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class NotificationTypeSettingServiceImpl implements NotificationTypeSettingService {

    @Autowired
    private final NotificationTypeRepository notificationTypeRepository;

    private final NotificationTypeSettingRepository notificationTypeSettingRepository;

    @Override
    public NotificationTypeSetting findByTypeIdAndEmployeeId(Long employeeId, Long notificationTypeId) {
        return null;
    }

    @Override
    public List<NotificationTypeSetting> findAllWithEmployeeId(Long employeeId) {
        return notificationTypeSettingRepository
            .findByEmployeeId(employeeId);
    }

    @Override
    public void createForEmployeeId(Employee employee) {
        List<NotificationType> notificationTypes = notificationTypeRepository.findAll();
        for (NotificationType notificationType : notificationTypes) {
            NotificationTypeSetting toSave = NotificationTypeSetting
                .builder()
                    .employeeId(employee.getId())
                    .employee(employee)
                    .notificationTypeId(notificationType.getId())
                    .notificationType(notificationType)
                    .build();
            save(toSave);
        }
    }

    @Override
    public NotificationTypeSetting save(NotificationTypeSetting nts) {
        return notificationTypeSettingRepository.save(nts);
    }

    @Override
    public void deleteAll(List<NotificationTypeSetting> ntsList) {
        notificationTypeSettingRepository.deleteAll(ntsList);
    }
}
