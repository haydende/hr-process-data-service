package com.haydende.hr_process.data_service.controller

import org.apache.commons.lang3.reflect.FieldUtils
import org.apache.commons.lang3.reflect.MethodUtils
import org.junit.Before
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.datasource.init.ScriptUtils
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import java.lang.reflect.Field
import java.sql.SQLException

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
@AutoConfigureMockMvc
class ControllerTest extends Specification {

    @Autowired MockMvc mockMvc
    @Autowired ModelMapper modelMapper

    static final boolean objectsEqual(Object object1, Object object2, Class aClass) {
        List<Field> fields = FieldUtils.getAllFields(aClass)
        boolean matches = false
        for (Field f: fields) {
            if (f.isSynthetic()) {
                continue
            }
            def methodName = 'get' + f.getName().substring(0,1).toUpperCase() + f.getName().substring(1)
            def object1Val = MethodUtils.invokeExactMethod(object1, methodName)
            def object2Val = MethodUtils.invokeExactMethod(object2, methodName)
            matches = Objects.equals(object1Val, object2Val)
        }
        return matches
    }

    final String unhappyGETRequest(String uri) {
        // when...
        def response = mockMvc.perform(
                get(uri))
                .andDo(print())

        // then verify the response and return as a boolean
        return response
            .andExpect(status().isNotFound())
            .andReturn()
                .getResponse()
                    .getContentAsString()
    }

    final String unhappyPOSTRequest(String uri, String jsonString) {
        // when the POST request has been sent
        def response = mockMvc.perform(
                post(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonString))
                .andDo(print())

        // then verify the response
        def result = response
            .andExpect(status().isBadRequest())
            .andReturn()

        // and get the error message
        return result.getResponse().getContentAsString()
    }

    final String unhappyPUTRequest(String uri, String jsonString, int expectedCode) {
        // when the PUT request has been sent
        def response = mockMvc.perform(
                put(uri)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(jsonString))
                .andDo(print())

        // then verify the response
        def result = response
            .andExpect(status().is(expectedCode))
            .andReturn()

        // and get the error message
        return result.getResponse().getContentAsString()
    }

    final String unhappyDELETERequest(String uri, int expectedStatus) {
        // WHEN the DELETE request has been sent
        def response = mockMvc.perform(
            delete(uri)
                .contentType(MediaType.APPLICATION_JSON))
            .andDo(print())

        // THEN verify the response
        def result = response
            .andExpect(status().is(expectedStatus))
            .andReturn()

        // AND get the error message
        return result.getResponse().getContentAsString()
    }

}
