package com.haydende.hr_process.data_service.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.haydende.hr_process.data_service.dto.DocumentDTO
import com.haydende.hr_process.data_service.model.Document
import com.haydende.hr_process.data_service.model.Employee
import com.haydende.hr_process.data_service.repository.DocumentAccessRepository
import com.haydende.hr_process.data_service.repository.DocumentRepository
import org.junit.Assert
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql

import java.sql.Timestamp

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyLong
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.verifyNoInteractions
import static org.mockito.Mockito.when
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class DocumentControllerTest extends ControllerTest {

    @Autowired DocumentRepository documentRepository
    @Autowired DocumentAccessRepository documentAccessRepository
    @Autowired ModelMapper modelMapper

    private static final String DELETE_TABLES = "/sql-scripts/delete-tables.sql"
    private static final String GET_MAPPING_SETUP = "/sql-scripts/document/get-mapping-setup.sql"
    private static final String GET_MAPPING_LIST_SETUP = "/sql-scripts/document/get-mapping-list-setup.sql"
    private static final String POST_MAPPING_SETUP = "/sql-scripts/document/post-mapping-setup.sql"

    private final Employee ownerEmployee = Employee.builder()
        .id(1L)
        .forename1("forename1")
        .forename2("forename2")
        .forename3("forename3")
        .lastname("lastname")
        .email("ffflastname@email.com")
        .officePhone("123456789")
        .mobilePhone("123456789")
        .password("password")
        .holidays(22)
        .jobTitle("Software Developer")
        .lineManager(null)
        .birthDate(new Timestamp(1000))
        .gender("male")
        .ethnicity("white/british")
        .religion("N/A")
        .disability("N/A")
        .createdDatetime(new Timestamp(2000))
        .documents([documentWithId])
        .notifications(null)
        .messageThreads(null)
        .build()

    private final DocumentDTO documentDtoWithoutId = DocumentDTO.builder()
        .documentType("documentType")
        .ownerEmployeeId(1L)
        .name("documentName")
        .content(new Byte[]{})
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    private final Document documentWithId = Document.builder()
        .id(1)
        .documentType("documentType")
        .ownerEmployee(ownerEmployee)
        .name("documentName")
        .content(new Byte[]{})
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    private final DocumentDTO documentDtoWithId = DocumentDTO.builder()
        .id(1L)
        .documentType("documentType")
        .ownerEmployeeId(1L)
        .name("documentName")
        .content(new Byte[]{})
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Document with ID - Happy Path'() {
        given:
        def documentId = 1L

        when:
        def response = mockMvc.perform(
                get("/document?id=$documentId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        DocumentDTO sentDocument = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), DocumentDTO.class)

        def actualDocument = documentRepository.findById(documentId)
        def actualDocumentDTO = modelMapper.map(actualDocument, DocumentDTO.class)
        objectsEqual(sentDocument, actualDocumentDTO, DocumentDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Document with ID - Unhappy Path'() {
        given:
        def documentId = 2L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/document?id=$documentId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No Document objects could be found with params: [ID: $documentId, OwnerEmployeeId: null]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Document> with matching OwnerEmployeeId - Happy Path'() {
        given:
        def ownerEmployeeId = 1L

        when:
        def response = mockMvc.perform(
                get("/document?owner=$ownerEmployeeId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        List<DocumentDTO> sentDocuments = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), new TypeReference<List<DocumentDTO>>(){})

        def actualDocuments = documentRepository.findAllByOwnerEmployeeId(ownerEmployeeId)
        def actualDocumentDTOs = actualDocuments.stream().map(doc -> modelMapper.map(doc, DocumentDTO.class)).toList()
        println("Size here ${actualDocumentDTOs.size()}")

        objectsEqual(sentDocuments.get(0), actualDocumentDTOs.get(0), DocumentDTO.class)
        objectsEqual(sentDocuments.get(1), actualDocumentDTOs.get(1), DocumentDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Document> with matching OwnerEmployeeId - Unhappy Path'() {
        given:
        def ownerEmployeeId = 2L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/document?owner=$ownerEmployeeId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No Document objects could be found with params: [ID: null, OwnerEmployeeId: $ownerEmployeeId]."
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Document - Happy Path'() {
        given:
        def documentToSendAsJson = new ObjectMapper().writeValueAsString(documentDtoWithoutId)

        when:
        def response = mockMvc.perform(
                post("/document/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(documentToSendAsJson))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        DocumentDTO sentDocumentDTO = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), DocumentDTO.class)

        def actualDocument = documentRepository.findById(sentDocumentDTO.getId())
        def actualDocumentDTO = modelMapper.map(actualDocument, DocumentDTO.class)
        objectsEqual(actualDocumentDTO, sentDocumentDTO, DocumentDTO.class)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Document - Unhappy Path'() {
        given: 'Document object to be sent will contain an ID value, indicating that it is not a new Document instance'
        def documentToSendAsJson = new ObjectMapper().writeValueAsString(documentWithId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPOSTRequest("/document/new", documentToSendAsJson)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Document object already has an ID value!"

        and: 'Check nothing has been saved via repositories'
        def actualDocuments = documentRepository.findAll()
        Assert.assertEquals(0, actualDocuments.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Document - Happy Path'() {
        given:
        def documentToSave = new ObjectMapper().writeValueAsString(documentDtoWithId)

        when:
        def response = mockMvc.perform(
                put("/document")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(documentToSave))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        DocumentDTO sentDocument = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), DocumentDTO.class)

        def actualDocument = documentRepository.findById(sentDocument.getId()).get()
        def actualDocumentDTO = modelMapper.map(actualDocument, DocumentDTO.class)
        objectsEqual(sentDocument, actualDocumentDTO, DocumentDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Document - Unhappy Path 1: Missing ID'() {
        given: 'Document object to be sent will contain no ID value, indicating that it is not an existing Document instance'
        def documentToSendAsJson = new ObjectMapper().writeValueAsString(documentDtoWithoutId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPUTRequest("/document", documentToSendAsJson, 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Document object has no ID value!"

        and: 'Check nothing has been saved via repositories'
        def actualDocuments = documentRepository.findAll()
        Assert.assertEquals(1, actualDocuments.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Document - Unhappy Path 2: Incorrect ID'() {
        given: 'Document object to be sent will contain an incorrect ID value'
        def documentWithIncorrectId = documentDtoWithId
        documentWithIncorrectId.id = 2

        def documentToSendAsJson = new ObjectMapper().writeValueAsString(documentWithIncorrectId)

        when: 'Send POST request and expect 404 Not Found'
        def result = unhappyPUTRequest("/document", documentToSendAsJson, 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "Received Document object has an unrecognised ID value!"

        and: 'Check nothing has been saved via repositories'
        def actualDocuments = documentRepository.findAll()
        Assert.assertEquals(1, actualDocuments.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Document with ID - Happy Path'() {
        given:
        def documentId = 1L

        when:
        def response = mockMvc.perform(
                delete("/document?id=$documentId"))
                .andDo(print())

        then: 'Verify response'
        response
                .andExpect(status().isOk())

        and: 'Verify that the document record has been deleted'
        def actualDocuments = documentRepository.findAll()
        Assert.assertEquals(0, actualDocuments.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Document with ID - Unhappy Path 1: Incorrect ID'() {
        given:
        def documentId = 2L

        when: 'Send DELETE request and expect 404 Not Found'
        def result = unhappyDELETERequest("/document?id=$documentId", 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "No Document objects could be found with params: [ID: $documentId]."

        and: 'Assert that no documents have been deleted'
        def actualDocuments = documentRepository.findAll()
        actualDocuments.size() == 1
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Document with ID - Unhappy Path 2: Missing ID'() {
        given:

        when: 'Send DELETE request and expect 400 Bad Request'
        def result = unhappyDELETERequest("/document", 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Invalid request received. ID value is required."

        and: 'Assert that no documents have been deleted'
        def actualDocuments = documentRepository.findAll()
        actualDocuments.size() == 1
    }
}