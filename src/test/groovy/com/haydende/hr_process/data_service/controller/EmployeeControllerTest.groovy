package com.haydende.hr_process.data_service.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.haydende.hr_process.data_service.dto.EmployeeDTO
import com.haydende.hr_process.data_service.model.Employee
import com.haydende.hr_process.data_service.repository.EmployeeRepository
import com.haydende.hr_process.data_service.repository.NotificationTypeRepository
import com.haydende.hr_process.data_service.repository.NotificationTypeSettingRepository
import org.h2.tools.Server
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.jupiter.api.AfterEach
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql

import java.sql.Timestamp

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyLong
import static org.mockito.Mockito.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
class EmployeeControllerTest extends ControllerTest {

    @Autowired ModelMapper modelMapper
    @Autowired EmployeeRepository employeeRepository
    @Autowired NotificationTypeSettingRepository ntsRepository
    @Autowired NotificationTypeRepository ntRepository

    private static final String DELETE_TABLES = "/sql-scripts/delete-tables.sql"
    private static final String GET_MAPPING_SETUP = "/sql-scripts/employee/get-mapping-setup.sql"
    private static final String POST_MAPPING_SETUP = "/sql-scripts/employee/post-mapping-setup.sql"

    private final EmployeeDTO employeeWithoutId = EmployeeDTO.builder()
        .forename1("forename1")
        .forename2("forename2")
        .forename3("forename3")
        .lastname("lastname")
        .email("flastname@email.com")
        .mobilePhone("284739127340812734")
        .officePhone(null)
        .holidays(22)
        .jobTitle("Job Title")
        .lineManagerId(null)
        .birthDate(new Timestamp(100))
        .gender("gender")
        .ethnicity("ethnicity")
        .religion("religion")
        .disability("disability")
        .build()

    private final EmployeeDTO employeeWithId = EmployeeDTO.builder()
        .id(1)
        .forename1("forename1")
        .forename2("forename2")
        .forename3("forename3")
        .lastname("lastname")
        .email("flastname@email.com")
        .mobilePhone("284739127340812734")
        .officePhone(null)
        .holidays(22)
        .jobTitle("Job Title")
        .lineManagerId(null)
        .birthDate(new Timestamp(100))
        .gender("gender")
        .ethnicity("ethnicity")
        .religion("religion")
        .disability("disability")
        .build()

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Employee with ID - Happy Path'() {
        setup:
        def employeeId = 1L

        when:
        def response = mockMvc.perform(
                get("/employee?id=$employeeId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        EmployeeDTO sentEmployee = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), EmployeeDTO.class)

        def actualEmployeeDTO =
            modelMapper.map(
                employeeRepository.findById(employeeId).find(),
                EmployeeDTO.class
            )
        objectsEqual(sentEmployee, actualEmployeeDTO, EmployeeDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Employee with ID - Unhappy Path'() {
        setup:
        def employeeId = 2L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/employee?id=$employeeId")

        then: 'Response should be 404 Not Found and message returned'
        result == "No Employee objects could be found with params: [ID: $employeeId, Name: null, Email: null]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Employee with Email - Happy Path'() {
        setup:
        def emailString = "jbtSmith@email.com"

        when:
        def response = mockMvc.perform(
                get("/employee?email=$emailString"))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        List<EmployeeDTO> sentEmployees = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), new TypeReference<List<EmployeeDTO>>() {})
        EmployeeDTO actualEmployeeDTO = modelMapper.map(employeeRepository.findById(1L).find(), EmployeeDTO.class)
        objectsEqual(sentEmployees.get(0), actualEmployeeDTO, EmployeeDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET User with Email - Unhappy Path'() {
        setup:
        def emailString = "flastname%40email.com"

        when: 'Send GET Request and expect 404 Not Found'
        def result = unhappyGETRequest("/employee?email=$emailString")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No Employee objects could be found with params: [ID: null, Name: null, Email: $emailString]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET List<Employee> with Name - Happy Path'() {
        setup:
        def nameString = "John"

        when:
        def response = mockMvc.perform(
                get("/employee?name=$nameString"))
                .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        List<EmployeeDTO> receivedEmployeeDTOs = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), new TypeReference<List<EmployeeDTO>>() {})
        EmployeeDTO actualEmployeeDTO = modelMapper.map(employeeRepository.findById(1L).find(), EmployeeDTO.class)
        objectsEqual(receivedEmployeeDTOs.get(0), actualEmployeeDTO, EmployeeDTO.class)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Employee - Happy Path'() {
        setup:
        def employeeToSendAsJson = new ObjectMapper().writeValueAsString(employeeWithoutId)

        when:
        def response = mockMvc.perform(
            post("/employee/new")
                .contentType(MediaType.APPLICATION_JSON)
                .content(employeeToSendAsJson))
            .andDo(print())

        then: 'Verify the response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        def savedEmployeeDTO = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), new TypeReference<EmployeeDTO>() {})
        def actualEmployeeDTO = modelMapper.map(employeeRepository.findById(savedEmployeeDTO.getId()).get(), EmployeeDTO.class)
        objectsEqual(savedEmployeeDTO, actualEmployeeDTO, EmployeeDTO.class)

        and: 'Assert that the new Employee object has been saved'
        def actualEmployees = employeeRepository.findAll()
        Assert.assertEquals(1, actualEmployees.size())

        and: 'Assert that NotificationTypeSettings for this Employee have also been created'
        def actualNotificationTypeSettings = ntsRepository.findByEmployeeId(savedEmployeeDTO.getId())
        Assert.assertTrue(actualNotificationTypeSettings.size() > 0)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Employee - Unhappy Path'() {
        setup:
        def employeeToSendAsJson = new ObjectMapper().writeValueAsString(employeeWithId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPOSTRequest("/employee/new", employeeToSendAsJson)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Employee object already has an ID value!"

        and: 'Assert that no new Employee objects have been saved'
        def actualEmployees = employeeRepository.findAll()
        Assert.assertEquals(0, actualEmployees.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Employee - Happy Path'() {
        setup:
        def employeeToSave = new ObjectMapper().writeValueAsString(employeeWithId)

        when:
        def response = mockMvc.perform(
                put("/employee")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(employeeToSave))
                .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        EmployeeDTO sentEmployee = new ObjectMapper()
            .readValue(
                    result.getResponse().getContentAsString(),
                    EmployeeDTO.class)

        def actualEmployee = employeeRepository.findById(sentEmployee.getId()).get()
        def actualEmployeeDTO = modelMapper.map(actualEmployee, EmployeeDTO.class)
        objectsEqual(sentEmployee, actualEmployeeDTO, EmployeeDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Employee - Unhappy Path 1: Missing ID'() {
        setup:
        def employeeToSendAsJson = new ObjectMapper().writeValueAsString(employeeWithoutId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPUTRequest("/employee", employeeToSendAsJson, 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Invalid request received. ID value is required."

        and: 'Assert that no new Employee objects have been saved'
        def actualEmployees = employeeRepository.findAll()
        Assert.assertEquals(1, actualEmployees.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Employee - Unhappy Path 2: Incorrect ID'() {
        setup:
        def employeeWithIncorrectId = employeeWithId
        employeeWithIncorrectId.id = 2

        def employeeToSendAsJson = new ObjectMapper().writeValueAsString(employeeWithIncorrectId)

        when: 'Send POST request and expect 404 Not Found'
        def result = unhappyPUTRequest("/employee", employeeToSendAsJson, 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "Received Employee object has an unrecognised ID value!"

        and: 'Assert that no new Employee objects have been saved'
        def actualEmployees = employeeRepository.findAll()
        Assert.assertEquals(1, actualEmployees.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Employee with ID - Happy Path'() {
        setup:
        def employeeId = 1L

        when:
        def response = mockMvc.perform(
                delete("/employee?id=$employeeId"))
                .andDo(print())

        then: 'Verify response'
        response
                .andExpect(status().isOk())

        and: 'Assert that the Employee record has been removed'
        def actualEmployee = employeeRepository.findById(employeeId)
        actualEmployee.isEmpty()
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Employee with ID - Unhappy Path 1: Incorrect ID'() {
        setup:
        def employeeId = 2

        when: 'Send DELETE request and expect 404 Not Found'
        def result = unhappyDELETERequest("/employee?id=${employeeId}", 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "No Employee objects could be found with params: [ID: $employeeId]."

        and: 'Assert that the existing Employee object has not been deleted'
        def actualEmployees = employeeRepository.findAll()
        Assert.assertEquals(1, actualEmployees.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Employee with ID - Unhappy Path 2: Missing ID'() {
        when: 'Send DELETE request and expect 400 Bad Request'
        def result = unhappyDELETERequest("/employee", 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Invalid request received. ID value is required."
    }
}
