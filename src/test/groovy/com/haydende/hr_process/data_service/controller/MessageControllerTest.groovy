package com.haydende.hr_process.data_service.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.haydende.hr_process.data_service.dto.MessageDTO
import com.haydende.hr_process.data_service.model.Message
import com.haydende.hr_process.data_service.model.MessageThread
import com.haydende.hr_process.data_service.model.id.MessageId
import com.haydende.hr_process.data_service.repository.MessageRepository
import com.haydende.hr_process.data_service.repository.MessageThreadRepository
import org.junit.Assert
import org.modelmapper.ModelMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql
import java.sql.Timestamp

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@AutoConfigureMockMvc
class MessageControllerTest extends ControllerTest {

    @Autowired MessageRepository messageRepository
    @Autowired MessageThreadRepository messageThreadRepository

    private static final String DELETE_TABLES = "/sql-scripts/delete-tables.sql"
    private static final String GET_MAPPING_SETUP = "/sql-scripts/message/get-mapping-setup.sql"
    private static final String GET_MAPPING_LIST_SETUP = "/sql-scripts/message/get-mapping-list-setup.sql"
    private static final String POST_MAPPING_SETUP = "/sql-scripts/message/post-mapping-setup.sql"

    private final MessageDTO messageDtoWithoutId = MessageDTO.builder()
        .id(null)
        .messageThreadId(1L)
        .content("messageContent")
        .build()

    private final MessageDTO messageDtoWithId = MessageDTO.builder()
        .id(1L)
        .messageThreadId(1L)
        .content("messageContent")
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Message with MessageThreadId and ID - Happy Path'() {
        given:
        def messageThreadId = 1L
        def messageId = 1L

        when:
        def response = mockMvc.perform(
                get("/message?messageThread=$messageThreadId&id=$messageId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        MessageDTO receivedMessage = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), MessageDTO.class)
        def messageIdObject = new MessageId(receivedMessage.getId(), receivedMessage.getMessageThreadId())
        def actualMessage = messageRepository.findByMessageId(messageIdObject)
        def actualMessageDTO = modelMapper.map(actualMessage, MessageDTO.class)

        objectsEqual(actualMessageDTO, receivedMessage, MessageDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Message with MessageThreadId and ID - Unhappy Path 1: Incorrect MessageThreadId and Id combination'() {
        given:
        def messageThreadId = 2L
        def messageId = 1L

        when: 'Send GET Request and expect 404 Not Found'
        def result = unhappyGETRequest(
            "/message?messageThread=$messageThreadId&id=$messageId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No Message objects could be found with params: [ID: $messageId, MessageThreadId: $messageThreadId]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Message> with matching messageThreadId - Happy Path'() {
        given:
        def messageThreadId = 1L

        when:
        def response = mockMvc.perform(
                get("/message?messageThread=$messageThreadId"))
                .andDo(print())

        then:
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and:
        List<MessageDTO> receivedMessages = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), new TypeReference<List<MessageDTO>>() {})

        def actualMessageList = messageRepository.findAllByMessageThreadId(messageThreadId)
        def actualMessageDtoList = actualMessageList
            .stream().map((message) -> modelMapper.map(message, MessageDTO.class)).toList()

        Assert.assertEquals(actualMessageList.size(), receivedMessages.size())
        for (int i = 0; i < receivedMessages.size(); i++) {
            objectsEqual(actualMessageDtoList.get(i), receivedMessages.get(i), MessageDTO.class)
        }
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Message> with matching messageThreadId - Unhappy Path'() {
        given:
        def messageThreadId = 2L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/message?messageThread=$messageThreadId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No Message objects could be found with params: [ID: null, MessageThreadId: $messageThreadId]."
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Message - Happy Path'() {
        given:
        def messageToSendAsJson = new ObjectMapper().writeValueAsString(messageDtoWithoutId)

        when:
        def response = mockMvc.perform(
            post("/message/new")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(messageToSendAsJson))
            .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        MessageDTO receivedMessage = new ObjectMapper()
            .readValue(
                    result.getResponse().getContentAsString(),
                    MessageDTO.class)
        def messageIdObject = new MessageId(receivedMessage.getId(), receivedMessage.getMessageThreadId())
        def actualSavedMessage = messageRepository.findByMessageId(messageIdObject)
        def actualSavedMessageDTO = modelMapper.map(actualSavedMessage, MessageDTO.class)

        objectsEqual(actualSavedMessageDTO, receivedMessage, MessageDTO.class)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Message - Unhappy Path'() {
        given: 'Message object to be sent will contain an ID value, indicating that it is not a new Message instance'
        def messageToSendAsJson = new ObjectMapper().writeValueAsString(messageDtoWithId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPOSTRequest("/message/new", messageToSendAsJson)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Message object already has an ID value!"

        and: 'Check nothing has been saved via repositories'
        def actualMessages = messageRepository.findAll()
        Assert.assertEquals(0, actualMessages.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Message - Happy Path'() {
        given:
        def messageToSave = new ObjectMapper().writeValueAsString(messageDtoWithId)
        def messageThreadId = 1L
        def messageId = 1L

        when:
        def response = mockMvc.perform(
                put("/message?messageThread=$messageThreadId&id=$messageId")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(messageToSave))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        MessageDTO receivedMessage = new ObjectMapper()
                .readValue(
                        result.getResponse().getContentAsString(),
                        MessageDTO.class)
        def messageIdObject = new MessageId(messageId, messageThreadId)
        def actualMessage = messageRepository.findByMessageId(messageIdObject)
        def actualMessageDTO = modelMapper.map(actualMessage, MessageDTO.class)

        objectsEqual(actualMessageDTO, receivedMessage, MessageDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Message - Unhappy Path 1: Missing ID'() {
        given:
        def messageId= 1L
        def messageToSendAsJson = new ObjectMapper().writeValueAsString(messageDtoWithoutId)

        when:
        def response = mockMvc
            .perform(
                put("/message?id=$messageId")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(messageToSendAsJson)
            ).andDo(print())

        then:
        response
            .andExpect(status().isBadRequest())

            .andReturn()

        and:
        def actualMessages = messageRepository.findAll()
        Assert.assertEquals(1, actualMessages.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Message - Unhappy Path 2: Incorrect ID'() {
        given: 'Request to be sent will include message object with incorrect ID value'
        def messageThreadId = 1L
        def messageId = 2L
        def messageWithIncorrectId = messageDtoWithId
        messageWithIncorrectId.id=messageId
        messageWithIncorrectId.messageThreadId=messageThreadId

        def messageToSendAsJson = new ObjectMapper().writeValueAsString(messageWithIncorrectId)

        when: 'Send POST request and expect 400 Bad Request'
        def response = mockMvc
            .perform(
                put("/message")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(messageToSendAsJson)
            ).andDo(print())

        then: 'Validate the response'
        def errorMessage = response
            .andExpect(status().isNotFound())
            .andReturn()
                .getResponse()
                    .getContentAsString()

        then: 'Response should be 400 Bad Request with matching error message'
        errorMessage == "Received Message object has an unrecognised ID value!"

        and: 'Check nothing has been saved via repositories'
        def actualMessages = messageRepository.findAll()
        Assert.assertEquals(1, actualMessages.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Message with ID - Happy Path'() {
        given:
        def messageThreadId = 1L
        def messageId = 1L

        when:
        def response = mockMvc.perform(
                delete("/message?messageThread=$messageThreadId&id=$messageId"))
                .andDo(print())

        then: 'Verify response'
        response
                .andExpect(status().isOk())

        and: 'Verify that the delete method has been called'
        def actualMessages = messageRepository.findAll()
        Assert.assertEquals(0, actualMessages.size())
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Message with ID - Unhappy Path'() {
        given: 'The mock Message repository will return null when getting the Message with ID, indicating it does not exist'
        def messageThreadId = 1L
        def messageId = 2L

        when:
        def result = unhappyDELETERequest("/message?messageThread=$messageThreadId&id=$messageId", 404)

        then:
        result == "No Message objects could be found with params: [ID: $messageId, MessageThreadId: $messageThreadId]."

        and: 'Verify that the existing message has not been deleted'
        def actualMessages = messageRepository.findAll()
        Assert.assertEquals(1, actualMessages.size())
    }
}
