package com.haydende.hr_process.data_service.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.haydende.hr_process.data_service.dto.MessageThreadDTO
import com.haydende.hr_process.data_service.model.MessageThread
import com.haydende.hr_process.data_service.repository.MessageThreadRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.jdbc.Sql
import java.sql.Timestamp

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.http.MediaType.APPLICATION_JSON

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class MessageThreadControllerTest extends ControllerTest {

    @Autowired MessageThreadRepository messageThreadRepository
    
    private static final String DELETE_TABLES = "/sql-scripts/delete-tables.sql"
    private static final String GET_MAPPING_SETUP = "/sql-scripts/message-thread/get-mapping-setup.sql"
    private static final String GET_MAPPING_LIST_SETUP = "/sql-scripts/message-thread/get-mapping-list-setup.sql"
    private static final String POST_MAPPING_SETUP = "/sql-scripts/message-thread/post-mapping-setup.sql"
    
    private final MessageThreadDTO messageThreadDtoWithoutId = MessageThreadDTO.builder()
        .subject("subject")
        .lastModifiedDatetime(new Timestamp(2000))
        .createdDatetime(new Timestamp(1000))
        .build()

    private final MessageThreadDTO messageThreadDtoWithId = MessageThreadDTO.builder()
        .id(1L)
        .subject("subject")
        .lastModifiedDatetime(new Timestamp(3000))
        .createdDatetime(new Timestamp(3000))
        .build()
    
    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET MessageThread with ID - Happy Path'() {
        given:
        def messageThreadId = 1L

        when:
        def response = mockMvc.perform(
                get("/message-thread?id=$messageThreadId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        MessageThreadDTO sentMessageThreadDTO = new ObjectMapper()
                .readValue(result.getResponse().getContentAsString(), MessageThreadDTO.class)

        sentMessageThreadDTO.getId() == 1L
        sentMessageThreadDTO.getSubject() == "Message subject"
        sentMessageThreadDTO.getCreatedDatetime().toString() == "2022-05-30 00:00:00.0"
        sentMessageThreadDTO.getLastModifiedDatetime().toString() == "2022-05-30 00:01:12.0"
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET MessageThread with ID - Unhappy Path'() {
        given:
        def messageThreadId = 2L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/message-thread?id=$messageThreadId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No MessageThread objects could be found with params: [ID: $messageThreadId, EmployeeId: null]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<MessageThread> with EmployeeId - Happy Path'() {
        given:
        def employeeId = 1L

        when:
        def response = mockMvc.perform(
                get("/message-thread?employee=$employeeId"))
                .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        List<MessageThreadDTO> sentMessageThreadDTOs = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), new TypeReference<List<MessageThreadDTO>>() {})

        def firstMessageThreadDTO = sentMessageThreadDTOs.get(0)
        firstMessageThreadDTO.getId() == 1L
        firstMessageThreadDTO.getSubject() == "Message subject"
        firstMessageThreadDTO.getCreatedDatetime().toString() == "2022-05-30 00:00:00.0"
        firstMessageThreadDTO.getLastModifiedDatetime().toString() == "2022-05-30 00:01:12.0"

        def secondMessageThreadDTO = sentMessageThreadDTOs.get(1)
        secondMessageThreadDTO.getId() == 2L
        secondMessageThreadDTO.getSubject() == "Second message subject"
        secondMessageThreadDTO.getCreatedDatetime().toString() == "2023-05-30 00:00:00.0"
        secondMessageThreadDTO.getLastModifiedDatetime().toString() == "2023-05-30 00:01:12.0"
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<MessageThread> with EmployeeId - Unhappy Path'() {
        given:
        def employeeId = 3L

        when: 'Send GET request and expect 404 Not Found'
        def result = unhappyGETRequest("/message-thread?employee=$employeeId")

        then: 'Response should be 404 Not Found and boolean returned'
        result == "No MessageThread objects could be found with params: [ID: null, EmployeeId: $employeeId]."
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new MessageThread - Happy Path'() {
        given:
        def messageThreadToSendAsJson = new ObjectMapper().writeValueAsString(messageThreadDtoWithoutId)

        when:
        def response = mockMvc.perform(
                post("/message-thread/new")
                        .contentType(APPLICATION_JSON)
                        .content(messageThreadToSendAsJson))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        MessageThreadDTO sentMessageThreadDTO = new ObjectMapper()
                .readValue(
                        result.getResponse().getContentAsString(),
                        MessageThreadDTO.class)

        MessageThread actualMessageThread = messageThreadRepository.findById(sentMessageThreadDTO.getId()).get()
        MessageThreadDTO actualMessageThreadDTO = modelMapper.map(actualMessageThread, MessageThreadDTO.class)

        objectsEqual(sentMessageThreadDTO, actualMessageThreadDTO, MessageThreadDTO.class)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new MessageThread - Unhappy Path'() {
        given: 'MessageThread object to be sent will contain an ID value, indicating that it is not a new MessageThread instance'
        def messageThreadToSendAsJson = new ObjectMapper().writeValueAsString(messageThreadDtoWithId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPOSTRequest("/message-thread/new", messageThreadToSendAsJson)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received MessageThread object already has an ID value!"

        and: 'Check nothing has been saved via repositories'
        List<MessageThread> actualMessageThreads = messageThreadRepository.findAll()
        actualMessageThreads.size() == 0
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated MessageThread - Happy Path'() {
        given:
        def messageThreadToSave = new ObjectMapper().writeValueAsString(messageThreadDtoWithId)

        when:
        def response = mockMvc.perform(
                put("/message-thread")
                        .contentType(APPLICATION_JSON)
                        .content(messageThreadToSave))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        MessageThreadDTO sentMessageThreadDTO = new ObjectMapper()
                .readValue(
                        result.getResponse().getContentAsString(),
                        MessageThreadDTO.class)

        def actualMessageThread = messageThreadRepository.findById(sentMessageThreadDTO.getId()).get()
        def actualMessageThreadDTO = modelMapper.map(actualMessageThread, MessageThreadDTO.class)

        objectsEqual(sentMessageThreadDTO, actualMessageThreadDTO, MessageThreadDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated MessageThread - Unhappy Path 1: Missing ID'() {
        given: 'MessageThread object to be sent will contain no ID value, indicating that it is not an existing MessageThread instance'
        def messageThreadToSendAsJson = new ObjectMapper().writeValueAsString(messageThreadDtoWithoutId)

        when: 'Send POST request an expect 400 Bad Request'
        def result = unhappyPUTRequest("/message-thread", messageThreadToSendAsJson, 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received MessageThread object has no ID value!"

        and: 'Verify that the updated fields in the new object haven\'t been saved'
        def actualMessageThread = messageThreadRepository.findById(1L).get()
        def actualMessageThreadDTO = modelMapper.map(actualMessageThread, MessageThreadDTO.class)

        !objectsEqual(messageThreadDtoWithoutId, actualMessageThreadDTO, MessageThreadDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated MessageThread - Unhappy Path 2: Incorrect ID'() {
        given: 'MessageThread object to be sent will contain an incorrect ID value'
        def messageThreadWithIncorrectId = messageThreadDtoWithId
        messageThreadWithIncorrectId.id = 2

        def messageThreadToSendAsJson = new ObjectMapper().writeValueAsString(messageThreadWithIncorrectId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPUTRequest("/message-thread", messageThreadToSendAsJson, 404)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received MessageThread object has an unrecognised ID value!"

        and: 'Verify that the updated fields in the new object haven\'t been saved'
        def actualMessageThread = messageThreadRepository.findById(1L).get()
        def actualMessageThreadDTO = modelMapper.map(actualMessageThread, MessageThreadDTO.class)

        !objectsEqual(messageThreadDtoWithoutId, actualMessageThreadDTO, MessageThreadDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE MessageThread with ID - Happy Path'() {
        given:
        def messageThreadId = 1L

        when:
        def response = mockMvc.perform(
                delete("/message-thread?id=$messageThreadId"))
                .andDo(print())

        then: 'Verify response'
        response
                .andExpect(status().isOk())

        and: 'Verify that the delete method has been called'
        messageThreadRepository.findAll().size() == 0
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE MessageThread with ID - Unhappy Path'() {
        given: 'The mock MessageThread repository will return null when getting the MessageThread'
        def messageThreadId = 2L

        when: 'Send DELETE request and expect 400 Bad Request'
        def result = unhappyDELETERequest("/message-thread?id=$messageThreadId", 404)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "No MessageThread objects could be found with params: [ID: $messageThreadId, EmployeeId: null]."

        and: 'Verify that the no objects have been deleted'
        messageThreadRepository.findAll().size() == 1
    }

}
