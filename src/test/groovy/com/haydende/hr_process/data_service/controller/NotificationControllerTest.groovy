package com.haydende.hr_process.data_service.controller

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.haydende.hr_process.data_service.dto.NotificationDTO
import com.haydende.hr_process.data_service.model.Notification
import com.haydende.hr_process.data_service.model.NotificationType
import com.haydende.hr_process.data_service.repository.EmployeeNotificationRepository
import com.haydende.hr_process.data_service.repository.NotificationRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.jdbc.Sql

import java.sql.Timestamp

import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyLong
import static org.mockito.Mockito.*
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class NotificationControllerTest extends ControllerTest {

    @Autowired NotificationRepository notificationRepository
    @Autowired EmployeeNotificationRepository employeeNotificationRepository

    private static final String DELETE_TABLES = "/sql-scripts/delete-tables.sql"
    private static final String GET_MAPPING_SETUP = "/sql-scripts/notification/get-mapping-setup.sql"
    private static final String GET_MAPPING_LIST_SETUP = "/sql-scripts/notification/get-mapping-list-setup.sql"
    private static final String POST_MAPPING_SETUP = "/sql-scripts/notification/post-mapping-setup.sql"

    private final NotificationDTO notificationDtoWithoutId = NotificationDTO.builder()
        .notificationTypeId(1L)
        .title("notificationTitle")
        .content("notificationContent")
        .published(false)
        .publishedDatetime(new Timestamp(1000))
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    private final NotificationDTO notificationDtoWithId = NotificationDTO.builder()
        .id(1L)
        .notificationTypeId(1L)
        .title("notificationTitle")
        .content("notificationContent")
        .published(false)
        .publishedDatetime(new Timestamp(1000))
        .createdDatetime(new Timestamp(1000))
        .lastModifiedDatetime(new Timestamp(1000))
        .build()

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Notification with ID - Happy Path'() {
        given:
        def notificationId = 1L

        when:
        def response = mockMvc.perform(
            get("/notification?id=$notificationId"))
            .andDo(print())

        then: 'Verify response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Validate the response body'
        NotificationDTO sentNotificationDTO = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), NotificationDTO.class)
        def actualNotificationDTO = modelMapper.map(notificationRepository.findById(notificationId).get(), NotificationDTO.class)

        objectsEqual(sentNotificationDTO, actualNotificationDTO, NotificationDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'GetMapping - GET Notification with ID - Unhappy Path'() {
        given:
        def notificationId = 2L

        when:
        def result = unhappyGETRequest("/notification?id=$notificationId")

        then:
        result == "No Notification objects could be found with params: [ID: $notificationId, EmployeeId: null]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Notification> with EmployeeId - Happy Path'() {
        given:
        def employeeId = 1L

        when:
        def response = mockMvc.perform(
                get("/notification?employee=$employeeId"))
                .andDo(print())

        then: 'Validate response'
        def result = response
            .andExpect(status().isOk())
            .andReturn()

        and: 'Verify the response body'
        List<NotificationDTO> sentNotifications = new ObjectMapper()
            .readValue(result.getResponse().getContentAsString(), new TypeReference<List<NotificationDTO>>() {})

        List<NotificationDTO> actualNotificationDTOs = notificationRepository.findAll()
            .stream()
            .map(notification -> modelMapper.map(notification, NotificationDTO.class))
            .toList()

        objectsEqual(sentNotifications.get(0), actualNotificationDTOs.get(0), NotificationDTO.class)
        objectsEqual(sentNotifications.get(1), actualNotificationDTOs.get(1), NotificationDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'GetMapping - GET List<Notification> with EmployeeID - Unhappy Path'() {
        given:
        def employeeId = 3L

        when:
        def result = unhappyGETRequest("/notification?employee=$employeeId")

        then:
        result == "No Notification objects could be found with params: [ID: null, EmployeeId: $employeeId]."
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Notification - Happy Path'() {
        given:
        def notificationToSendAsJson = new ObjectMapper().writeValueAsString(notificationDtoWithoutId)

        when:
        def response = mockMvc.perform(
                post("/notification/new")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(notificationToSendAsJson))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        NotificationDTO sentNotification = new ObjectMapper()
                .readValue(
                        result.getResponse().getContentAsString(),
                        NotificationDTO.class)
        def actualNotificationDTO = modelMapper.map(notificationRepository.findById(sentNotification.getId()).find(), NotificationDTO.class)

        objectsEqual(sentNotification, actualNotificationDTO, NotificationDTO.class)
    }

    @Sql([DELETE_TABLES, POST_MAPPING_SETUP])
    def 'PostMapping - POST new Notification - Unhappy Path'() {
        given: 'Notification object to be sent will contain an ID value, indicating that it is not a new Notification instance'
        def notificationToSendAsJson = new ObjectMapper().writeValueAsString(notificationDtoWithId)

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPOSTRequest("/notification/new", notificationToSendAsJson)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Notification object already has an ID value!"

        and: 'Check nothing has been saved via repositories'
        notificationRepository.findAll().size() == 0
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Notification - Happy Path'() {
        given:
        def notificationToSave = new ObjectMapper().writeValueAsString(notificationDtoWithId)

        when:
        def response = mockMvc.perform(
                put("/notification")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(notificationToSave))
                .andDo(print())

        then: 'Verify response'
        def result = response
                .andExpect(status().isOk())
                .andReturn()

        and: 'Validate the response body'
        NotificationDTO sentNotification = new ObjectMapper()
                .readValue(
                        result.getResponse().getContentAsString(),
                        NotificationDTO.class)

        def actualNotificationDTO = modelMapper.map(notificationRepository.findById(sentNotification.getId()).get(), NotificationDTO.class)

        objectsEqual(sentNotification, actualNotificationDTO, NotificationDTO.class)
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Notification - Unhappy Path 1: Missing ID Value'() {
        given: 'Notification object to be sent will contain no ID value, indicating that it is not an existing Notification instance'
        def notificationToSendAsJson = new ObjectMapper().writeValueAsString(notificationDtoWithoutId)
        def lastModifiedDateTimeBefore = notificationRepository.findById(1L).get().lastModifiedDatetime

        when: 'Send POST request and expect 400 Bad Request'
        def result = unhappyPUTRequest("/notification", notificationToSendAsJson, 400)

        then: 'Response should be 400 Bad Request with matching error message'
        result == "Received Notification object has no ID value!"

        and: 'Check nothing has been saved via repositories'
        def lastModifiedDateTimeAfter = notificationRepository.findById(1L).get().lastModifiedDatetime
        lastModifiedDateTimeBefore == lastModifiedDateTimeAfter
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'PutMapping - PUT updated Notification - Unhappy Path 2: Incorrect ID Value'() {
        given: 'Notification object to be sent will contain an incorrect ID value'
        def notificationWithIncorrectId = notificationDtoWithId
        notificationWithIncorrectId.id = 4L

        def notificationToSendAsJson = new ObjectMapper().writeValueAsString(notificationWithIncorrectId)
        def lastModifiedDateTimeBefore = notificationRepository.findById(1L).get().lastModifiedDatetime

        when: 'Send POST request and expect 404 Not Found'
        def result = unhappyPUTRequest("/notification", notificationToSendAsJson, 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "Received Notification object has an unrecognised ID value!"

        and: 'Check nothing has been saved via repositories'
        def lastModifiedDateTimeAfter = notificationRepository.findById(1L).get().lastModifiedDatetime
        lastModifiedDateTimeBefore == lastModifiedDateTimeAfter
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Notification with ID - Happy Path'() {
        given:
        def notificationId = 1L

        when:
        def response = mockMvc.perform(
            delete("/notification?id=$notificationId"))
            .andDo(print())

        then: 'Verify response'
        response
            .andExpect(status().isOk())

        and: 'Verify that the delete method has been called'
        notificationRepository.findAll().size() == 0
    }

    @Sql([DELETE_TABLES, GET_MAPPING_SETUP])
    def 'DeleteMapping - DELETE Notification with ID - Unhappy Path'() {
        given: 'The mock Notification repository will return null when getting the Notification with ID, indicating it does not exist'
        def notificationId = 4L

        when: 'Send DELETE request and expect 404 Not Found'
        def result = unhappyDELETERequest("/notification?id=$notificationId", 404)

        then: 'Response should be 404 Not Found with matching error message'
        result == "No Notification objects could be found with params: [ID: $notificationId, EmployeeId: null]."
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'DeleteMapping - DELETE Notifications for Employee - Happy Path 1: Single EmployeeNotification'() {
        given:
        def employeeId = 1L

        when:
        def response =
            mockMvc.perform(
                delete("/notification?employee=$employeeId")
            ).andDo(print())

        then: 'Verify response'
        response
            .andExpect(status().isOk())

        and: 'Verify that the employee_notification entry has been deleted and not any notification records'
        employeeNotificationRepository.findAll()
            .stream()
            .filter(employeeNotification -> employeeNotification.employeeNotificationId.employeeId == employeeId)
            .toList()
            .size() == 0

        notificationRepository.findAll().size() == 2
    }

    @Sql([DELETE_TABLES, GET_MAPPING_LIST_SETUP])
    def 'DeleteMapping - DELETE Notifications for Employee - Happy Path 2: Multiple EmployeeNotifications'() {
        given:
        def employeeId = 1L

        when:
        def response =
            mockMvc.perform(
                delete("/notification?employee=$employeeId")
            ).andDo(print())

        then: 'Verify response'
        response
            .andExpect(status().isOk())

        and: 'Verify that only one employee_notification has been deleted and not any notifications/other employee_notification'
        employeeNotificationRepository.findAll().size() == 2
        notificationRepository.findAll().size() == 2
    }
}
