package com.haydende.hr_process.data_service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
class HrProcessDataServiceApplicationTests {

	@Test
	void contextLoads() {
	}

}
