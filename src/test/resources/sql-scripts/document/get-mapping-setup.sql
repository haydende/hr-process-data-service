
DELETE FROM document_access;
DELETE FROM document;
DELETE FROM employee;

INSERT INTO employee (id, forename1, forename2, forename3, lastname, email, office_phone, mobile_phone, password, holidays, job_title, line_manager_id, birth_date, gender, created_datetime)
VALUES (1, 'John', 'Brian', 'Trevor', 'Smith', 'jbtSmith@email.com', '123456789', '123456789', 'password123', 25, 'Accountant', null, '1970-01-01', 'male', '2022-01-01T00:00:00');

INSERT INTO document (id, document_type, owner_employee_id, name, content, created_datetime, last_modified_datetime)
VALUES (1, 'Contract', 1, 'John Smith Contract', 0x6e6576657220676f6e6e61206769766520796f75207570, '2022-01-01T00:00:00', '2022-01-01T00:00:00');

INSERT INTO document_access (document_id, employee_id, contributed)
VALUES (1, 1, true);
