
DELETE FROM document_access;
DELETE FROM document;
DELETE FROM employee;

INSERT INTO employee (id, forename1, forename2, forename3, lastname, email, office_phone, mobile_phone, password, holidays, job_title, line_manager_id, birth_date, gender, created_datetime)
VALUES (1, 'John', 'Brian', 'Trevor', 'Smith', 'jbtSmith@email.com', '123456789', '123456789', 'password123', 25, 'Accountant', null, '1970-01-01', 'male', '2022-01-01T00:00:00');

