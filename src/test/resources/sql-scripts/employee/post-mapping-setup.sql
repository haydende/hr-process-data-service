DELETE FROM notification_type_setting;
DELETE FROM notification_type;
DELETE FROM employee;

INSERT INTO notification_type (id, name)
VALUES (1, 'Document updated'), (2, 'Document deleted'), (3, 'Credentials updated');
