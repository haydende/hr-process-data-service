DELETE FROM message_thread_employee;
DELETE FROM message;
DELETE FROM message_thread;
DELETE FROM employee;

INSERT INTO employee (id, forename1, forename2, forename3, lastname, email, office_phone, mobile_phone, password, holidays, job_title, line_manager_id, birth_date, gender, created_datetime)
VALUES (1, 'John', 'Brian', 'Trevor', 'Smith', 'jbtSmith@email.com', '123456789', '123456789', 'password123', 25, 'Accountant', null, '1970-01-01', 'male', '2022-01-01T00:00:00');

INSERT INTO message_thread (id, subject, created_datetime, last_modified_datetime)
VALUES (1, 'Message subject', '2022-05-30T00:00:00.000000000Z', '2022-05-30T00:01:12.000000000Z');

INSERT INTO message_thread_employee (message_thread_id, employee_id)
VALUES (1, 1);

INSERT INTO message (id, message_thread_id, content, created_datetime, last_modified_datetime)
VALUES (1, 1, 'Never gonna give you up', '2022-05-30T00:00:00.000000000Z', '2022-05-30T00:01:12.000000000Z');