
INSERT INTO employee (id, forename1, forename2, forename3, lastname, email, office_phone, mobile_phone, password, holidays, job_title, line_manager_id, birth_date, gender, created_datetime)
VALUES (1, 'John', 'Brian', 'Trevor', 'Smith', 'jbtSmith@email.com', '123456789', '123456789', 'password123', 25, 'Accountant', null, '1970-01-01', 'male', '2022-01-01T00:00:00');

INSERT INTO notification_type (id, name)
VALUES (1, 'Document updated'), (2, 'Document deleted'), (3, 'Credentials updated');

INSERT INTO notification_type_setting (notification_type_id, employee_id)
VALUES (1, 1), (2, 1), (3, 1);