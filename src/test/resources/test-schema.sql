DROP TABLE IF EXISTS `notification_type_setting`;
DROP TABLE IF EXISTS `employee_notification`;
DROP TABLE IF EXISTS `notification`;
DROP TABLE IF EXISTS `notification_type`;
DROP TABLE IF EXISTS `document_access`;
DROP TABLE IF EXISTS `document`;
DROP TABLE IF EXISTS `message`;
DROP TABLE IF EXISTS `message_thread_employee`;
DROP TABLE IF EXISTS `message_thread`;
DROP TABLE IF EXISTS `employee_notification`;
DROP TABLE IF EXISTS `employee`;

CREATE TABLE `employee` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `forename1` varchar(255) NOT NULL,
  `forename2` varchar(255) DEFAULT NULL,
  `forename3` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255),
  `office_phone` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `holidays` smallint(6),
  `job_title` varchar(255) NOT NULL,
  `line_manager_id` bigint(20) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `gender` varchar(255) DEFAULT 'they/them',
  `ethnicity` varchar(255) DEFAULT 'N/A',
  `religion` varchar(255) DEFAULT 'N/A',
  `disability` varchar(255) DEFAULT 'N/A',
  `created_datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `line_manager_id` (`line_manager_id`),
  CONSTRAINT `employee_ibfk_1` FOREIGN KEY (`line_manager_id`) REFERENCES `employee` (`id`)
);

CREATE TABLE `document` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `document_type` varchar(255) DEFAULT NULL,
  `owner_employee_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `content` blob DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY (`owner_employee_id`),
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`owner_employee_id`) REFERENCES `employee` (`id`)
);

CREATE TABLE `message_thread` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `subject` varchar(255) DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `message` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message_thread_id` bigint(20) DEFAULT NULL,
  `content` MEDIUMTEXT DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `message_thread_id` (`message_thread_id`),
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`message_thread_id`) REFERENCES `message_thread` (`id`)
);


CREATE TABLE `message_thread_employee` (
  `message_thread_id` bigint(20) NOT NULL,
  `employee_id` bigint(20) NOT NULL,
  PRIMARY KEY (`message_thread_id`,`employee_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `message_thread_employee_ibfk_1` FOREIGN KEY (`message_thread_id`) REFERENCES `message_thread` (`id`),
  CONSTRAINT `message_thread_employee_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
);

CREATE TABLE `document_access` (
    `document_id` bigint(20) NOT NULL,
    `employee_id` bigint(20) NOT NULL,
    `contributed` bit(1) DEFAULT 0,
    PRIMARY KEY (`document_id`, `employee_id`),
    CONSTRAINT `document_access_ibfk_1` FOREIGN KEY (`document_id`) REFERENCES `document` (`id`),
    CONSTRAINT `document_access_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
);

CREATE TABLE `notification_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `notification_type_setting` (
  `employee_id` bigint(20) NOT NULL,
  `notification_type_id` bigint(20) NOT NULL,
  `use_client` bit(1) DEFAULT 1,
  `use_email` bit(1) DEFAULT 1,
  `use_phone` bit(1) DEFAULT 0,
  PRIMARY KEY (`employee_id`, `notification_type_id`),
  CONSTRAINT `notification_type_setting_ibkf_1` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`),
  CONSTRAINT `notification_type_setting_ibkf_2` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_type` (`id`)
);

CREATE TABLE `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notification_type_id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` varchar(2000) DEFAULT NULL,
  `published` bit(1) DEFAULT 0,
  `published_datetime` datetime DEFAULT NULL,
  `created_datetime` datetime NOT NULL,
  `last_modified_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`notification_type_id`) REFERENCES `notification_type` (`id`)
);

CREATE TABLE `employee_notification` (
  `notification_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `employee_id` bigint(20) NOT NULL,
  PRIMARY KEY (`notification_id`,`employee_id`),
  CONSTRAINT `employee_notification_ibfk_1` FOREIGN KEY (`notification_id`) REFERENCES `notification` (`id`),
  CONSTRAINT `employee_notification_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`id`)
);